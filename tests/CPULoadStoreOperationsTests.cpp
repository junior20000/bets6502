#include <gtest/gtest.h>

#include <vector>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502InstructionsLoadAndStoreTests : public bets6502Test {};

TEST_F(bets6502InstructionsLoadAndStoreTests,
       LDAWhenCalledUpdateAccumulatorValue) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_lda(&cpu);

  ASSERT_EQ(cpu.acc, 10);
}

TEST_F(bets6502InstructionsLoadAndStoreTests, LDAWhenCalledSetZeroCorrectly) {
  betsBus_write(cpu.bus, 0, 0);
  bets6502_lda(&cpu);

  EXPECT_TRUE(bets6502_get_status(&cpu, kZ));

  betsBus_write(cpu.bus, 0, 3);
  bets6502_lda(&cpu);

  EXPECT_FALSE(bets6502_get_status(&cpu, kZ));
}

TEST_F(bets6502InstructionsLoadAndStoreTests,
       LDAWhenCalledSetNegativeCorrectly) {
  std::vector<uint8_t> inputs = {0x00, 0x79, 0x80, 0xFF};
  std::vector<bool> expected_values = {false, false, true, true};

  for (uint16_t x = 0; x < 4; x++) {
    betsBus_write(cpu.bus, 0, inputs[x]);
    bets6502_lda(&cpu);
    EXPECT_EQ(bets6502_get_status(&cpu, kN), expected_values[x]);
  }
}

TEST_F(bets6502InstructionsLoadAndStoreTests, LDAWhenCalledTickOneTime) {
  EXPECT_EQ(cpu.cycles, 0);

  betsBus_write(cpu.bus, 0, 0);
  bets6502_lda(&cpu);

  EXPECT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502InstructionsLoadAndStoreTests, LDXWhenCalledUpdateXValue) {
  betsBus_write(cpu.bus, 0, 10);
  bets6502_ldx(&cpu);

  EXPECT_EQ(cpu.x, 10);
}

TEST_F(bets6502InstructionsLoadAndStoreTests, LDXWhenCalledSetZeroCorrectly) {
  betsBus_write(cpu.bus, 0, 0);
  bets6502_ldx(&cpu);

  EXPECT_TRUE(bets6502_get_status(&cpu, kZ));

  betsBus_write(cpu.bus, 0, 3);
  bets6502_ldx(&cpu);

  EXPECT_FALSE(bets6502_get_status(&cpu, kZ));
}

TEST_F(bets6502InstructionsLoadAndStoreTests,
       LDXWhenCalledSetNegativeCorrectly) {
  std::vector<uint8_t> inputs = {0x00, 0x79, 0x80, 0xFF};
  std::vector<bool> expected_values = {false, false, true, true};

  for (uint16_t x = 0; x < 4; x++) {
    betsBus_write(cpu.bus, 0, inputs[x]);
    bets6502_ldx(&cpu);
    EXPECT_EQ(bets6502_get_status(&cpu, kN), expected_values[x]);
  }
}

TEST_F(bets6502InstructionsLoadAndStoreTests, LDXWhenCalledTickOneTime) {
  EXPECT_EQ(cpu.cycles, 0);

  betsBus_write(cpu.bus, 0, 0);
  bets6502_ldx(&cpu);

  EXPECT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502InstructionsLoadAndStoreTests,
       STAShouldWriteAccumulatorIntoMemory) {
  cpu.acc = 10;

  bets6502_sta(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502InstructionsLoadAndStoreTests,
       STXShouldWriteXRegisterIntoMemory) {
  cpu.x = 10;

  bets6502_stx(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502InstructionsLoadAndStoreTests,
       STYShouldWriteXRegisterIntoMemory) {
  cpu.y = 10;

  bets6502_sty(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
}