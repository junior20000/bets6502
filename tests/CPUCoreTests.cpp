#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502CoreTest : public bets6502Test {};

TEST_F(bets6502CoreTest, CPUReadAndWriteShouldReturnDataAtGivenAddress) {
  bets6502_write(&cpu, 0xFFFC, 0x12);
  ASSERT_EQ(cpu.cycles, 1);

  ASSERT_EQ(bets6502_read(&cpu, 0xFFFC), 0x12);
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502CoreTest, CPUReadAndWriteU16ShouldReturnDataAtGivenAddress) {
  bets6502_writeU16(&cpu, 0xFFFC, (uint16_t)0x1234);
  ASSERT_EQ(cpu.cycles, 2);

  ASSERT_EQ(bets6502_readU16_s(&cpu, 0xFFFC), 0x1234);
  ASSERT_EQ(cpu.cycles, 4);

  ASSERT_EQ(cpu.bus->mem->data[0xFFFC], 0x34);
  ASSERT_EQ(cpu.bus->mem->data[0xFFFD], 0x12);
}

TEST_F(bets6502CoreTest, StackU8ShouldWorkCorrectly) {
  uint8_t base = 0xFD;
  cpu.stkptr = base;

  for (uint8_t i = 1; i < 11; i++) {
    bets6502_spush(&cpu, i);
    ASSERT_EQ(cpu.stkptr, base - i);
    ASSERT_EQ(cpu.cycles, i);
  }

  ASSERT_EQ(cpu.stkptr, base - 10);
  ASSERT_EQ(cpu.cycles, 10);

  cpu.cycles = 0;

  for (uint8_t i = 10; (int8_t)i > 0; i--) {
    ASSERT_EQ(bets6502_spop(&cpu), i);
    ASSERT_EQ(cpu.stkptr, (base - i) + 1);
    ASSERT_EQ(cpu.cycles, 11 - i);
  }

  ASSERT_EQ(cpu.stkptr, base);
  ASSERT_EQ(cpu.cycles, 10);
}

TEST_F(bets6502CoreTest, StackU16ShouldWorkCorrectly) {
  uint8_t base = 0xFD;
  cpu.stkptr = base;

  for (uint16_t i = 2; i < 22; i += 2) {
    bets6502_spushU16(&cpu, ((uint16_t)(i << 8) + i));
    ASSERT_EQ(cpu.stkptr, base - i);
    ASSERT_EQ(cpu.cycles, i);
  }

  ASSERT_EQ(cpu.stkptr, base - 20);
  ASSERT_EQ(cpu.cycles, 20);

  cpu.cycles = 0;

  for (uint16_t i = 20; (int16_t)i > 0; i -= 2) {
    ASSERT_EQ(bets6502_spopU16(&cpu), ((uint16_t)(i << 8) + i));
    ASSERT_EQ(cpu.stkptr, (base - i) + 2);
    ASSERT_EQ(cpu.cycles, 22 - i);
  }

  ASSERT_EQ(cpu.stkptr, base);
  ASSERT_EQ(cpu.cycles, 20);
}

TEST_F(bets6502CoreTest, ResetShouldResetCPU) {
  betsBus_write(cpu.bus, 0xFFFC, 0x34);
  betsBus_write(cpu.bus, 0xFFFD, 0x12);

  bets6502_reset(&cpu);

  ASSERT_EQ(cpu.mar, 0xFFFC);
  ASSERT_EQ(cpu.pc, (uint16_t)0x1234);
  ASSERT_EQ(cpu.stkptr, 0xFD);
  ASSERT_STATUS_EQ(cpu.status, kU | kI);
  ASSERT_EQ(cpu.cycles, 7);
}

TEST_F(bets6502CoreTest, IRQShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0xFFFC, 0x78);
  betsBus_write(cpu.bus, 0xFFFD, 0x56);
  betsBus_write(cpu.bus, 0xFFFE, 0x34);
  betsBus_write(cpu.bus, 0xFFFF, 0x12);

  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, false);
  cpu.cycles = 0;

  bets6502_irq(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kI);

  ASSERT_EQ(cpu.mar, 0xFFFE);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 6);

  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kU);
  ASSERT_EQ(bets6502_spopU16(&cpu), 0x5678);
}

TEST_F(bets6502CoreTest, IRQShouldNotWorkIfInterruptFlagIsSet) {
  betsBus_write(cpu.bus, 0xFFFC, 0x34);
  betsBus_write(cpu.bus, 0xFFFD, 0x12);

  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, true);
  cpu.cycles = 0;

  bets6502_irq(&cpu);

  ASSERT_EQ(cpu.mar, 0xFFFC);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 0);
}

TEST_F(bets6502CoreTest, NMIShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0xFFFC, 0x78);
  betsBus_write(cpu.bus, 0xFFFD, 0x56);
  betsBus_write(cpu.bus, 0xFFFA, 0x34);
  betsBus_write(cpu.bus, 0xFFFB, 0x12);

  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, false);
  cpu.cycles = 0;

  bets6502_nmi(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kI);

  ASSERT_EQ(cpu.mar, 0xFFFA);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 6);

  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kU);
  ASSERT_EQ(bets6502_spopU16(&cpu), 0x5678);

  // NMI should trigger even if interrupt flag is set
  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, true);
  cpu.cycles = 0;

  bets6502_nmi(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kI);

  ASSERT_EQ(cpu.mar, 0xFFFA);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 6);

  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kU | kI);
  ASSERT_EQ(bets6502_spopU16(&cpu), 0x5678);
}