#include <gtest/gtest.h>
#include <stdlib.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502IllegalOpcodeTest : public bets6502Test {};

TEST_F(bets6502IllegalOpcodeTest, ALRShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 8);
  cpu.acc = 10;

  bets6502_alr(&cpu);

  ASSERT_EQ(cpu.acc, 4);
  ASSERT_EQ(cpu.cycles, 1);

  betsBus_write(cpu.bus, 0, 5);
  cpu.acc = 0xF;

  bets6502_alr(&cpu);

  ASSERT_EQ(cpu.acc, 2);

  betsBus_write(cpu.bus, 0, 0xFE);
  cpu.acc = 0xFF;

  bets6502_alr(&cpu);

  ASSERT_EQ(cpu.acc, 0x7F);
}

TEST_F(bets6502IllegalOpcodeTest, ALRShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 8);
  cpu.acc = 10;

  bets6502_alr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  betsBus_write(cpu.bus, 0, 5);
  cpu.acc = 0xF;

  bets6502_alr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 1);
  cpu.acc = 0xF;

  bets6502_alr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  cpu.status = kU | kC;

  betsBus_write(cpu.bus, 0, 0xFE);
  cpu.acc = 0xFF;

  bets6502_alr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502IllegalOpcodeTest, ANCShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 8);
  cpu.acc = 0xF;

  bets6502_anc(&cpu);

  ASSERT_EQ(cpu.acc, 8);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IllegalOpcodeTest, ANCShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x80);
  cpu.acc = 0xFF;

  bets6502_anc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  betsBus_write(cpu.bus, 0, 0x01);
  cpu.acc = 0xFE;

  bets6502_anc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  betsBus_write(cpu.bus, 0, 0x71);
  cpu.acc = 0x7F;

  bets6502_anc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502IllegalOpcodeTest, ARRWithoutDecimalModeShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x40);
  cpu.acc = 0xF0;

  bets6502_set_status(&cpu, kD, false);

  bets6502_arr(&cpu);

  ASSERT_EQ(cpu.acc, 0x20);
  ASSERT_EQ(cpu.cycles, 1);

  // Carry | Bit 7 | Bit 6
  uint8_t input[][3] = {{0, 0, 0}, {0, 0, 1}, {0, 1, 0}, {0, 1, 1},
                        {1, 0, 0}, {1, 0, 1}, {1, 1, 0}, {1, 1, 1}};

  // Carry | Overflow | Bit 7 | Bit 6
  uint8_t output[][4] = {{0, 0, 0, 0}, {0, 1, 0, 0}, {1, 1, 0, 1},
                         {1, 0, 0, 1}, {0, 0, 1, 0}, {0, 1, 1, 0},
                         {1, 1, 1, 1}, {1, 0, 1, 1}};

  cpu.status = kU;

  for (int i = 0; i < 8; i++) {
    uint8_t ic = input[i][0];
    uint8_t ib7 = (uint8_t)(input[i][1] << 7);
    uint8_t ib6 = (uint8_t)(input[i][2] << 6);

    uint8_t oc = output[i][0];
    uint8_t ov = output[i][1];
    uint8_t ob7 = (uint8_t)(output[i][2] << 7);
    uint8_t ob6 = (uint8_t)(output[i][3] << 6);

    cpu.acc = 0xC0;
    betsBus_write(cpu.bus, 0, 0x0F | (ib7 | ib6));

    bets6502_set_status(&cpu, kC, ic);

    bets6502_arr(&cpu);

    ASSERT_EQ(cpu.acc & 0x80, ob7);
    ASSERT_EQ(cpu.acc & 0x40, ob6);
    ASSERT_EQ(cpu.status & kC, oc);
    ASSERT_EQ(bets6502_get_status(&cpu, kV), (bool)ov);
    ASSERT_EQ(bets6502_get_status(&cpu, kN), (bool)ic);
  }
}

TEST_F(bets6502IllegalOpcodeTest, ARRDecimalShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x82);
  cpu.acc = 0x87;

  cpu.status = kU | kD;
  bets6502_arr(&cpu);

  ASSERT_EQ(cpu.acc, 0xA1);

  betsBus_write(cpu.bus, 0, 0x82);
  cpu.acc = 0x87;

  cpu.status = kU | kD | kC;
  bets6502_arr(&cpu);

  ASSERT_EQ(cpu.acc, 0x21);

  betsBus_write(cpu.bus, 0, 0xF0);
  cpu.acc = 0xFF;

  cpu.status = kU | kD;
  bets6502_arr(&cpu);

  ASSERT_EQ(cpu.acc, 0xD8);
}

TEST_F(bets6502IllegalOpcodeTest, ARRDecimalShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x82);
  cpu.acc = 0x87;

  cpu.status = kU | kD;
  bets6502_arr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kV | kU | kD | kC);

  betsBus_write(cpu.bus, 0, 0x82);
  cpu.acc = 0x87;

  cpu.status = kU | kD | kC;
  bets6502_arr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kV | kU | kD | kC);

  betsBus_write(cpu.bus, 0, 0xF0);
  cpu.acc = 0xFF;

  cpu.status = kU | kD;
  bets6502_arr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kC);

  betsBus_write(cpu.bus, 0, 0x00);
  bets6502_arr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kD);

  bets6502_arr(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kZ);
}

TEST_F(bets6502IllegalOpcodeTest, DCPShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 1);
  cpu.acc = 0x81;

  bets6502_dcp(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0);
  ASSERT_EQ(cpu.acc, 0x81);
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502IllegalOpcodeTest, DCPShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 1);
  cpu.acc = 0x81;

  bets6502_dcp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  betsBus_write(cpu.bus, 0, 0x01);
  cpu.acc = 0x01;

  bets6502_dcp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0x81);
  cpu.acc = 0x80;

  bets6502_dcp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);
}

TEST_F(bets6502IllegalOpcodeTest, ISCBinaryShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0xE);

  cpu.status = kU;
  cpu.acc = 0xFF;

  bets6502_isc(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xF);
  ASSERT_EQ(cpu.acc, 0xEF);

  betsBus_write(cpu.bus, 0, 0xE);

  cpu.status = kU | kC;
  cpu.acc = 0xFF;

  bets6502_isc(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xF);
  ASSERT_EQ(cpu.acc, 0xF0);
}

TEST_F(bets6502IllegalOpcodeTest, ISCBinaryShouldUpdateNVZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0xFC);

  cpu.acc = 0xFA;

  bets6502_isc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  betsBus_write(cpu.bus, 0, 0xF);

  bets6502_isc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);
}

TEST_F(bets6502IllegalOpcodeTest, ISCWithBCDShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x1F);

  cpu.status = kU | kD;
  cpu.acc = 0x80;

  bets6502_isc(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x20);
  ASSERT_EQ(cpu.acc, 0x59);

  betsBus_write(cpu.bus, 0, 0x24);

  cpu.status = kU | kD | kC;
  cpu.acc = 0x30;

  bets6502_isc(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x25);
  ASSERT_EQ(cpu.acc, 0x05);
}

TEST_F(bets6502IllegalOpcodeTest, ISCWithInvalidBCDShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x1E);

  cpu.status = kU | kD | kC;
  cpu.acc = 0xAE;

  bets6502_isc(&cpu);

  ASSERT_EQ(cpu.acc, 0x89);

  betsBus_write(cpu.bus, 0, 0xAB);

  cpu.status = kU | kD;
  cpu.acc = 0xFA;

  bets6502_isc(&cpu);

  ASSERT_EQ(cpu.acc, 0x47);
}

TEST_F(bets6502IllegalOpcodeTest, ISCBCDShouldUpdateNVZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0xAB);

  cpu.status = kU | kD;
  cpu.acc = 0xFA;

  bets6502_isc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kC);
}

TEST_F(bets6502IllegalOpcodeTest, JAMShouldWorkAsIntended) {
  bets6502_jam(&cpu);

  ASSERT_EQ(cpu.mar, 0xFF);
  ASSERT_EQ(cpu.mdr, 0xFF);
  ASSERT_TRUE(cpu.is_jammed);
}

TEST_F(bets6502IllegalOpcodeTest, LAXShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x69);

  bets6502_lax(&cpu);

  ASSERT_EQ(cpu.acc, 0x69);
  ASSERT_EQ(cpu.x, 0x69);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IllegalOpcodeTest, RLAShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x8E);

  cpu.status = kU | kC;

  cpu.acc = 0x18;
  bets6502_rla(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x1D);
  ASSERT_EQ(cpu.acc, 0x18);
  ASSERT_EQ(cpu.cycles, 3);

  betsBus_write(cpu.bus, 0, 0xEE);

  cpu.status = kU;

  cpu.acc = 0x0F;
  bets6502_rla(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xDC);
  ASSERT_EQ(cpu.acc, 0xC);
}

TEST_F(bets6502IllegalOpcodeTest, RLAShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x8E);

  cpu.acc = 0xFF;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0x40);

  cpu.acc = 0xF0;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  betsBus_write(cpu.bus, 0, 0);

  cpu.acc = 0;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502IllegalOpcodeTest, RRADecimalShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x80);

  cpu.status = kU | kC | kD;

  cpu.acc = 0x20;
  bets6502_rra(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xC0);
  ASSERT_EQ(cpu.acc, 0x40);
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502IllegalOpcodeTest, RRABinaryShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x80);

  cpu.status = kU | kC;

  cpu.acc = 0x20;
  bets6502_rra(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xC0);
  ASSERT_EQ(cpu.acc, 0xE0);
  ASSERT_EQ(cpu.cycles, 3);

  betsBus_write(cpu.bus, 0, 0x14);

  cpu.status = kU;

  cpu.acc = 0xA0;
  bets6502_rra(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0xA);
  ASSERT_EQ(cpu.acc, 0xAA);
}

TEST_F(bets6502IllegalOpcodeTest, RRAShouldUpdateNVZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x8E);

  cpu.acc = 0xFF;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0x40);

  cpu.acc = 0xF0;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  betsBus_write(cpu.bus, 0, 0);

  cpu.acc = 0;
  bets6502_rla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502IllegalOpcodeTest, SAXShouldWorkAsIntended) {
  cpu.x = 0x83;
  cpu.acc = 0xAE;

  bets6502_sax(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x82);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IllegalOpcodeTest, SBXShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x70);

  cpu.x = 0xFF;
  cpu.acc = 0xF0;

  bets6502_sbx(&cpu);

  ASSERT_EQ(cpu.x, 0x80);
  ASSERT_EQ(cpu.acc, 0xF0) << "A should stay immutable";
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IllegalOpcodeTest, SBXShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x30);

  cpu.x = 0xFF;
  cpu.acc = 0x40;

  bets6502_sbx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0x30);

  cpu.x = 0x20;
  cpu.acc = 0xFF;

  bets6502_sbx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  betsBus_write(cpu.bus, 0, 0x70);

  cpu.x = 0xF0;

  bets6502_sbx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  betsBus_write(cpu.bus, 0, 0x80);

  bets6502_sbx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);
}

TEST_F(bets6502IllegalOpcodeTest, SLOShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0x40);

  cpu.acc = 0xF;
  bets6502_slo(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.acc, 0x8F);
}

TEST_F(bets6502IllegalOpcodeTest, SLOShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x40);

  cpu.acc = 0xF;
  bets6502_slo(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  betsBus_write(cpu.bus, 0, 0x80);

  cpu.acc = 0xF;
  bets6502_slo(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0);

  cpu.acc = 0;
  bets6502_slo(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502IllegalOpcodeTest, SREShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 0xAA);

  cpu.acc = 0xFF;
  bets6502_sre(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x55);
  ASSERT_EQ(cpu.acc, 0xAA);
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502IllegalOpcodeTest, SREShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0xAA);

  cpu.acc = 0xFF;
  bets6502_sre(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  cpu.acc = 0xFF;
  bets6502_sre(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC | kN);

  betsBus_write(cpu.bus, 0, 0);

  cpu.acc = 0;
  bets6502_sre(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}