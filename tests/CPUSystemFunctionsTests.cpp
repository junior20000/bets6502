#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

#define SET_NMI_NTH_TICK(TICK)                             \
  static void set_nmi_##TICK##th_tick(bets6502* mp) {      \
    if ((mp->cycles != 0) && (mp->cycles % (TICK)) == 0) { \
      mp->is_nmi_pending = true;                           \
    }                                                      \
  }

SET_NMI_NTH_TICK(1);
SET_NMI_NTH_TICK(2);
SET_NMI_NTH_TICK(3);
SET_NMI_NTH_TICK(4);
SET_NMI_NTH_TICK(5);

#undef SET_NMI_NTH_TICK

class bets6502SystemFunctionTest : public bets6502Test {};

TEST_F(bets6502SystemFunctionTest, BRKShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0xFFFC, 0x78);
  betsBus_write(cpu.bus, 0xFFFD, 0x56);
  betsBus_write(cpu.bus, 0xFFFE, 0x34);
  betsBus_write(cpu.bus, 0xFFFF, 0x12);

  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, false);
  cpu.cycles = 0;

  bets6502_brk(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kI);

  ASSERT_EQ(cpu.mar, 0xFFFE);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 6);

  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kU | kB);
  ASSERT_EQ(bets6502_spopU16(&cpu), 0x5679);

  // BRK should trigger even if interrupt flag is set
  bets6502_reset(&cpu);
  bets6502_set_status(&cpu, kI, true);
  cpu.cycles = 0;

  bets6502_brk(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kI);

  ASSERT_EQ(cpu.mar, 0xFFFE);
  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 6);

  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kU | kI | kB);
  ASSERT_EQ(bets6502_spopU16(&cpu), 0x5679);
}

TEST_F(bets6502SystemFunctionTest, BRKShouldBeHijackableDuringFirstFourTicks) {
  betsBus_write(cpu.bus, 0xFFFC, 0x78);
  betsBus_write(cpu.bus, 0xFFFD, 0x56);
  betsBus_write(cpu.bus, 0xFFFE, 0x78);
  betsBus_write(cpu.bus, 0xFFFF, 0x56);
  betsBus_write(cpu.bus, 0xFFFA, 0x34);
  betsBus_write(cpu.bus, 0xFFFB, 0x12);

  const bets6502_tick_logic tick_list[] = {&set_nmi_1th_tick, &set_nmi_2th_tick,
                                           &set_nmi_3th_tick,
                                           &set_nmi_4th_tick};

  for (int i = 4; i > 0; i--) {
    cpu.tick_logic = tick_list[i - 1];

    bets6502_reset(&cpu);
    cpu.cycles = 0;

    cpu.is_nmi_pending = false;

    bets6502_tick(&cpu);  // Emulate data fetch
    bets6502_brk(&cpu);

    ASSERT_EQ(cpu.mar, 0xFFFA)
        << "BRK must be hijackable during the first four cycles.";
    ASSERT_EQ(cpu.pc, 0x1234) << "The NMI vector was not read correctly.";
    ASSERT_EQ(cpu.cycles, 7);
  }

  /* BRK should work as normal if the signal is sent past the 4th tick */

  cpu.tick_logic = &set_nmi_5th_tick;

  bets6502_reset(&cpu);
  cpu.cycles = 0;

  cpu.is_nmi_pending = false;

  bets6502_tick(&cpu);
  bets6502_brk(&cpu);

  ASSERT_EQ(cpu.mar, 0xFFFE) << "BRK must not be hijackable if the signal is "
                                "set past the fourth cycle.";
  ASSERT_EQ(cpu.pc, 0x5678) << "The IRQ vector was not read correctly.";
  ASSERT_EQ(cpu.cycles, 7);
}

TEST_F(bets6502SystemFunctionTest, NOPShouldJustTick) {
  bets6502_nop(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502SystemFunctionTest, RTIShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0xFFFC, 0x78);
  betsBus_write(cpu.bus, 0xFFFD, 0x56);
  betsBus_write(cpu.bus, 0xFFFE, 0x34);
  betsBus_write(cpu.bus, 0xFFFF, 0x12);

  bets6502_reset(&cpu);
  bets6502_sec(&cpu);

  bets6502_brk(&cpu);

  cpu.cycles = 0;
  bets6502_rti(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kI | kC);
  ASSERT_EQ(cpu.cycles, 5);
}