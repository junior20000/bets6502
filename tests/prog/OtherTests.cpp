#include <gtest/gtest.h>
#include <stdlib.h>

#include "../utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502MiscProgramTest : public bets6502Test {};

// BigEd's timings test
TEST_F(bets6502MiscProgramTest, CpuTimingsShouldBeCorrect) {
  long long unsigned tinst = 0;
  long long unsigned tcycles = 0;

  constexpr size_t prog_start = 0x1000;
  constexpr size_t prog_end = 0x1269;

  bets6502_reset(&cpu);

  cpu.pc = prog_start;
  cpu.cycles = 0;

  FILE* prog;
  fopen_s(&prog, ".\\res\\timing_test.bin", "rb");

  if (!prog) {
    fputs("Could not find file timing_test.bin.", stderr);
    exit(EXIT_FAILURE);
  }

  fread(cpu.bus->mem->data + 0x1000, 1, 0x026C, prog);

  fclose(prog);

  do {
    cpu.ir = bets6502_read(&cpu, cpu.pc);
    const bets6502Instruction* op = &bets6502_instset[cpu.ir];
    cpu.pc++;

    op->mode(&cpu);
    op->inst(&cpu);

    tcycles += cpu.cycles;
    tinst++;
    cpu.cycles = 0;

    ASSERT_LE(tinst, 500)
        << "Too many instructions were executed.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

    ASSERT_LE(tcycles, 1141)
        << "The test took too many cycles. Please, check if the instruction "
           "timings are correct.\n"
           "  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

  } while (cpu.pc != prog_end);

  ASSERT_EQ(tcycles, 1141);
};