#include <gtest/gtest.h>
#include <stdlib.h>

#include "../utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502KlausTest : public bets6502Test {};

#ifdef BETS6502_WITH_DECIMAL_MODE

TEST_F(bets6502KlausTest, FunctionalTestWithDecimalModeWithoutInterrupts) {
  long long unsigned tinst = 0;
  long long unsigned tcycles = 0;

  constexpr size_t prog_start = 0x0400;
  constexpr size_t prog_end = 0x3469;

  bets6502_reset(&cpu);

  cpu.pc = prog_start;
  cpu.cycles = 0;

  FILE* prog;
  fopen_s(&prog, ".\\res\\6502_functional_test.bin", "rb");

  if (!prog) {
    fputs("Could not find file 6502_functional_test.bin.", stderr);
    exit(EXIT_FAILURE);
  }

  fread(cpu.bus->mem->data + 0x000A, 1, mem_sz, prog);

  fclose(prog);

  do {
    cpu.ir = bets6502_read(&cpu, cpu.pc);
    const bets6502Instruction* op = &bets6502_instset[cpu.ir];
    cpu.pc++;

    op->mode(&cpu);
    op->inst(&cpu);

    tcycles += cpu.cycles;
    tinst++;
    cpu.cycles = 0;

    ASSERT_LE(tinst, 30648048)
        << "Too many instructions were executed.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

    ASSERT_LE(tcycles, 96247422)
        << "The test took too many cycles. Please, check if the instruction "
           "timings are correct.\n If the timings are correct, something went "
           "wrong.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

  } while (cpu.pc != prog_end);

  ASSERT_EQ(tcycles, 96247422);
  ASSERT_EQ(tinst, 30648048);
};

#endif  // BETS6502_WITH_DECIMAL_MODE

TEST_F(bets6502KlausTest, InterruptsShouldWorkAsIntended) {
  constexpr uint8_t irq_bit = 1;
  constexpr uint8_t nmi_bit = 2;

  long long unsigned tinst = 0;
  long long unsigned tcycles = 0;

  constexpr size_t prog_start = 0x0400;
  constexpr size_t prog_end = 0x06f5;

  bets6502_reset(&cpu);

  cpu.pc = prog_start;
  cpu.cycles = 0;

  FILE* prog = nullptr;
  fopen_s(&prog, ".\\res\\6502_interrupt_test.bin", "rb");

  if (!prog) {
    fputs("Could not find file 6502_interrupt_test.bin.", stderr);
    exit(EXIT_FAILURE);
  }

  fread(cpu.bus->mem->data + 0x000A, 1, mem_sz, prog);

  fclose(prog);

  // Initialize the stack and the interrupt port
  do {
    cpu.ir = bets6502_read(&cpu, cpu.pc);
    const bets6502Instruction* op = &bets6502_instset[cpu.ir];
    cpu.pc++;

    op->mode(&cpu);
    op->inst(&cpu);

    tcycles += cpu.cycles;

  } while (cpu.pc != 0x0419);

  // Test start
  do {
    cpu.ir = bets6502_read(&cpu, cpu.pc);
    const bets6502Instruction* op = &bets6502_instset[cpu.ir];
    cpu.pc++;

    op->mode(&cpu);
    op->inst(&cpu);

    cpu.is_irq_pending = (bool)(cpu.bus->mem->data[0xbffc] & irq_bit);
    cpu.is_nmi_pending = (bool)(cpu.bus->mem->data[0xbffc] & nmi_bit);

    if (cpu.is_nmi_pending) {
      cpu.bus->mem->data[0xbffc] &= ~nmi_bit;
      bets6502_nmi(&cpu);
    } else if (cpu.is_irq_pending && !bets6502_get_status(&cpu, kI)) {
      cpu.bus->mem->data[0xbffc] &= ~irq_bit;
      bets6502_irq(&cpu);
    }

    tinst++;
    tcycles += cpu.cycles;
    cpu.cycles = 0;

    ASSERT_LE(tinst, 1026)
        << "Too many instructions were executed.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

    ASSERT_LE(tcycles, 3179)
        << "The test took too many cycles. Please, check if the instruction "
           "timings are correct.\n If the timings are correct, something went "
           "wrong.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

  } while (cpu.pc != prog_end);

  ASSERT_EQ(tcycles, 3179);
  ASSERT_EQ(tinst, 1026);
};

TEST_F(bets6502KlausTest, DecimalTestWithIllegalInputsAndFlagChecking) {
  long long unsigned tinst = 0;
  long long unsigned tcycles = 0;

  constexpr size_t prog_start = 0x0200;
  constexpr size_t prog_end = 0x025b;

  bets6502_reset(&cpu);

  cpu.pc = prog_start;
  cpu.cycles = 0;

  FILE* prog;
  fopen_s(&prog, ".\\res\\6502_decimal_test.bin", "rb");

  if (!prog) {
    fputs("Could not find file 6502_decimal_test.bin.", stderr);
    exit(EXIT_FAILURE);
  }

  fread(cpu.bus->mem->data + 0x0200, 1, 274, prog);

  fclose(prog);

  do {
    cpu.ir = bets6502_read(&cpu, cpu.pc);
    const bets6502Instruction* op = &bets6502_instset[cpu.ir];
    cpu.pc++;

    op->mode(&cpu);
    op->inst(&cpu);

    tcycles += cpu.cycles;
    tinst++;
    cpu.cycles = 0;

    ASSERT_LE(tinst, 2999283)
        << "Too many instructions were executed.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

    ASSERT_LE(tcycles, 9075081)
        << "The test took too many cycles. Please, check if the instruction "
           "timings are correct.\n If the timings are correct, something went "
           "wrong.\n  Last PC value: "
        << std::showbase << std::hex << cpu.pc
        << "\n  Last opcode executed: " << (uint16_t)cpu.ir << std::endl;

  } while (cpu.pc != prog_end);

  ASSERT_EQ(betsBus_read(cpu.bus, 0xb), 0);

  ASSERT_EQ(tcycles, 9075081);
  ASSERT_EQ(tinst, 2999283);
}