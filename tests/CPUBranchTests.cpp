#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502BranchTest : public bets6502Test {};

TEST_F(bets6502BranchTest, BCCShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kC, true);

  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BCC should not branch if C is set.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kC, false);

  cpu.cycles = 0;
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

// Assure that BCC is working properly, so we can test the branch base
// behavior.
TEST_F(bets6502BranchTest, BranchInstructionsShouldBranchCorrectly) {
  betsBus_write(cpu.bus, 0, 0x7F);

  bets6502_set_status(&cpu, kC, true);
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 0)
      << "Instruction did not respect the condition to branch.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kC, false);

  cpu.cycles = 0;
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 0x7F)
      << "Instruction did not respect the condition to branch or did not "
         "calculate correctly the end position.";
  ASSERT_EQ(cpu.cycles, 2);

  cpu.pc = 0xFF;
  cpu.cycles = 0;
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 0x17E)
      << "Bad end position calculation. Maybe because of the page crossing?";
  ASSERT_EQ(cpu.cycles, 3) << "Missing the extra tick due to page crossing or "
                              "bad page cross calculation.";

  betsBus_write(cpu.bus, 0, 0xFF);
  cpu.pc = 0xA;
  cpu.cycles = 0;
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 9)
      << "Bad end position calculation. Be sure to treat the sum as signed.";
  ASSERT_EQ(cpu.cycles, 2) << "Bad page cross calculation.";

  betsBus_write(cpu.bus, 0, 0xFF);
  cpu.pc = 0x100;
  cpu.cycles = 0;
  bets6502_bcc(&cpu);

  ASSERT_EQ(cpu.pc, 0xFF);
  ASSERT_EQ(cpu.cycles, 3) << "Bad page cross calculation.";
}

TEST_F(bets6502BranchTest, BCSShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kC, false);

  bets6502_bcs(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BCS should not branch if C is clear.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kC, true);

  cpu.cycles = 0;
  bets6502_bcs(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BEQShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kZ, false);

  bets6502_beq(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BEQ should not branch if Z is clear.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kZ, true);

  cpu.cycles = 0;
  bets6502_beq(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BMIShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kN, false);

  bets6502_bmi(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BMI should not branch if N is clear.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kN, true);

  cpu.cycles = 0;
  bets6502_bmi(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BNEShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kZ, true);

  bets6502_bne(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BNE should not branch if Z is set.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kZ, false);

  cpu.cycles = 0;
  bets6502_bne(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BPLShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kN, true);

  bets6502_bpl(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BPL should not branch if N is set.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kN, false);

  cpu.cycles = 0;
  bets6502_bpl(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BVCShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kV, true);

  bets6502_bvc(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BVC should not branch if V is set.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kV, false);

  cpu.cycles = 0;
  bets6502_bvc(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}

TEST_F(bets6502BranchTest, BVSShouldWorkCorrectly) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_set_status(&cpu, kV, false);

  bets6502_bvs(&cpu);

  ASSERT_EQ(cpu.pc, 0) << "BVS should not branch if V clear.";
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kV, true);

  cpu.cycles = 0;
  bets6502_bvs(&cpu);

  ASSERT_EQ(cpu.pc, 10) << "Bad end position calculation.";
  ASSERT_EQ(cpu.cycles, 2);
}