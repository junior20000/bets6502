#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502AddressingModesTest : public bets6502Test {};

TEST_F(bets6502AddressingModesTest, ImmediateAMShouldWorkAsExpected) {
  bets6502_am_imm(&cpu);

  ASSERT_FALSE(cpu.cycles);
  ASSERT_EQ(cpu.mar, 0);
  ASSERT_EQ(cpu.pc, 1);
}

TEST_F(bets6502AddressingModesTest, RelativeAMShouldWorkAsExpected) {
  bets6502_am_rel(&cpu);

  ASSERT_FALSE(cpu.cycles);
  ASSERT_EQ(cpu.mar, 0);
  ASSERT_EQ(cpu.pc, 1);
}

TEST_F(bets6502AddressingModesTest, AbsoluteAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x34);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  bets6502_am_abs(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x1234);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, ZeroPageAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0, 0x69);

  bets6502_am_zpg(&cpu);

  ASSERT_EQ(cpu.cycles, 1);
  ASSERT_EQ(cpu.mar, 0x0069);
  ASSERT_EQ(cpu.pc, 1);
}

TEST_F(bets6502AddressingModesTest, IndirectAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x69);
  betsBus_write(cpu.bus, 0x0001, 0x69);
  betsBus_write(cpu.bus, 0x6969, 0x34);
  betsBus_write(cpu.bus, 0x696A, 0x12);

  bets6502_am_ind(&cpu);

  ASSERT_EQ(cpu.cycles, 4);
  ASSERT_EQ(cpu.mar, 0x1234);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, IndirectAMShouldLoopAroundIfPageCrossed) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);
  betsBus_write(cpu.bus, 0x0001, 0x69);
  betsBus_write(cpu.bus, 0x6900, 0x12);
  betsBus_write(cpu.bus, 0x69FF, 0x34);

  bets6502_am_ind(&cpu);

  ASSERT_EQ(cpu.cycles, 4);
  ASSERT_EQ(cpu.mar, 0x1234);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, AbsoluteXAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x34);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  cpu.x = 6;

  bets6502_am_abx(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x123A);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, AbsoluteXAMShouldTickIfPageCross) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  cpu.x = 2;

  bets6502_am_abx(&cpu);

  ASSERT_EQ(cpu.cycles, 3);
  ASSERT_EQ(cpu.mar, 0x1301);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, AbsoluteXRMWAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x34);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  cpu.x = 6;

  bets6502_am_abx_rmw(&cpu);

  ASSERT_EQ(cpu.cycles, 3);
  ASSERT_EQ(cpu.mar, 0x123A);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, AbsoluteYAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x34);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  cpu.y = 6;

  bets6502_am_aby(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x123A);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, AbsoluteYAMShouldTickIfPageCross) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);
  betsBus_write(cpu.bus, 0x0001, 0x12);

  cpu.y = 2;

  bets6502_am_aby(&cpu);

  ASSERT_EQ(cpu.cycles, 3);
  ASSERT_EQ(cpu.mar, 0x1301);
  ASSERT_EQ(cpu.pc, 0x0002);
}

TEST_F(bets6502AddressingModesTest, ZeroPageXAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x69);

  cpu.x = 1;

  bets6502_am_zpx(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x006A);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, ZeroPageXAMShouldLoopAroundIfPageCross) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);

  cpu.x = 2;

  bets6502_am_zpx(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x0001);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, ZeroPageYAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x69);

  cpu.y = 1;

  bets6502_am_zpy(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x006A);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, ZeroPageYAMShouldLoopAroundIfPageCross) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);

  cpu.y = 2;

  bets6502_am_zpy(&cpu);

  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(cpu.mar, 0x0001);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, IndirectXAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x69);
  betsBus_write(cpu.bus, 0x006A, 0x34);
  betsBus_write(cpu.bus, 0x006B, 0x12);

  cpu.x = 1;

  bets6502_am_idx(&cpu);

  ASSERT_EQ(cpu.cycles, 4);
  ASSERT_EQ(cpu.mar, 0x1234);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, IndirectXAMShouldLoopAroundIfPageCross) {
  betsBus_write(cpu.bus, 0x0000, 0xFF);
  betsBus_write(cpu.bus, 0x0001, 0x34);
  betsBus_write(cpu.bus, 0x0002, 0x12);

  cpu.x = 2;

  bets6502_am_idx(&cpu);

  ASSERT_EQ(cpu.cycles, 4);
  ASSERT_EQ(cpu.mar, 0x1234);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest, IndirectYAMShouldWorkAsExpected) {
  betsBus_write(cpu.bus, 0x0000, 0x69);
  betsBus_write(cpu.bus, 0x0069, 0x34);
  betsBus_write(cpu.bus, 0x006A, 0x12);

  cpu.y = 1;

  bets6502_am_idy(&cpu);

  ASSERT_EQ(cpu.cycles, 3);
  ASSERT_EQ(cpu.mar, 0x1235);
  ASSERT_EQ(cpu.pc, 0x0001);
}

TEST_F(bets6502AddressingModesTest,
       IndirectYAMShouldWorkAsExpectedWhenPageCrossed) {
  betsBus_write(cpu.bus, 0x0000, 0x69);
  betsBus_write(cpu.bus, 0x0069, 0xFF);
  betsBus_write(cpu.bus, 0x006A, 0x12);

  cpu.y = 2;

  bets6502_am_idy(&cpu);

  ASSERT_EQ(cpu.cycles, 4);
  ASSERT_EQ(cpu.mar, 0x1301);
  ASSERT_EQ(cpu.pc, 0x0001);
}