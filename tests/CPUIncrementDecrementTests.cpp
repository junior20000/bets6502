#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502IncrementDecrementTest : public bets6502Test {};

TEST_F(bets6502IncrementDecrementTest, INCShouldIncrementTheMemory) {
  betsBus_write(cpu.bus, 0, 9);
  
  bets6502_inc(&cpu);

  ASSERT_EQ(cpu.mdr, 10);
  ASSERT_EQ(cpu.mdr, betsBus_read(cpu.bus, 0));
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502IncrementDecrementTest, INCShouldUpdateZNAsAppropriate) {
  betsBus_write(cpu.bus, 0, 9);

  bets6502_inc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  betsBus_write(cpu.bus, 0, 0xFF);

  bets6502_inc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  betsBus_write(cpu.bus, 0, 0x7F);

  bets6502_inc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502IncrementDecrementTest, INXShouldIncrementX) {
  cpu.x = 9;

  bets6502_inx(&cpu);

  ASSERT_EQ(cpu.x, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IncrementDecrementTest, INXShouldUpdateZNAsAppropriate) {
  cpu.x = 9;

  bets6502_inx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.x = 0xFF;

  bets6502_inx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.x = 0x7F;

  bets6502_inx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502IncrementDecrementTest, INYShouldIncrementY) {
  cpu.y = 9;

  bets6502_iny(&cpu);

  ASSERT_EQ(cpu.y, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IncrementDecrementTest, INYShouldUpdateZNAsAppropriate) {
  cpu.y = 9;

  bets6502_iny(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.y = 0xFF;

  bets6502_iny(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.y = 0x7F;

  bets6502_iny(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502IncrementDecrementTest, DECShouldDecrementTheMemory) {
  betsBus_write(cpu.bus, 0, 11);

  bets6502_dec(&cpu);

  ASSERT_EQ(cpu.mdr, 10);
  ASSERT_EQ(cpu.mdr, betsBus_read(cpu.bus, 0));
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502IncrementDecrementTest, DECShouldUpdateZNAsAppropriate) {
  betsBus_write(cpu.bus, 0, 11);

  bets6502_dec(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  betsBus_write(cpu.bus, 0, 0x01);

  bets6502_dec(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  betsBus_write(cpu.bus, 0, 0x00);

  bets6502_dec(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502IncrementDecrementTest, DEXShouldDecrementX) {
  cpu.x = 11;

  bets6502_dex(&cpu);

  ASSERT_EQ(cpu.x, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IncrementDecrementTest, DEXShouldUpdateZNAsAppropriate) {
  cpu.x = 11;

  bets6502_dex(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.x = 1;

  bets6502_dex(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.x = 0;

  bets6502_dex(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502IncrementDecrementTest, DEYShouldDecrementY) {
  cpu.y = 11;

  bets6502_dey(&cpu);

  ASSERT_EQ(cpu.y, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502IncrementDecrementTest, DEYShouldUpdateZNAsAppropriate) {
  cpu.y = 11;

  bets6502_dey(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.y = 1;

  bets6502_dey(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.y = 0;

  bets6502_dey(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}