#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502JumpCallTest : public bets6502Test {};

TEST_F(bets6502JumpCallTest, JMPShouldUpdatePCCorrectly) {
  cpu.mar = 0x1234;

  bets6502_jmp(&cpu);

  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 0);
}

TEST_F(bets6502JumpCallTest, JSRShouldUpdatePCCorrectly) {
  cpu.pc = 2;
  cpu.mar = 0x1234;

  bets6502_jsr(&cpu);

  ASSERT_EQ(cpu.pc, 0x1234);
  ASSERT_EQ(cpu.cycles, 3);
  ASSERT_EQ(bets6502_spopU16(&cpu), 1);
}

TEST_F(bets6502JumpCallTest, RTSShouldUpdatePCCorrectly) {
  cpu.pc = 2;
  cpu.mar = 0x1234;

  bets6502_jsr(&cpu);
  cpu.cycles = 0;

  bets6502_rts(&cpu);

  ASSERT_EQ(cpu.pc, 2);
  ASSERT_EQ(cpu.cycles, 5);
}