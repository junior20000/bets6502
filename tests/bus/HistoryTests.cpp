#include <bets6502/bus/history.h>

#pragma warning(push)
#pragma warning(disable : 26439 26495)
#include <gtest/gtest.h>
#pragma warning(pop)

class betsBusHistoryTest : public ::testing::Test {
 public:
  struct betsBusHistory* hist = nullptr;
  const size_t hist_sz = 0xFFFF;

 protected:
  void SetUp() override { hist = betsBusHistory_new(hist_sz); }

  void TearDown() override {
    free(hist);
    hist = nullptr;
  }
};

TEST(betsBusHistoryCoreTest, NewShouldAllocateGivenSizeAndSetThePtrsToZero) {
  size_t sz = 10;
  struct betsBusHistory* hist = betsBusHistory_new(sz);

  ASSERT_EQ(hist->sz, sz);
  ASSERT_EQ(hist->rptr, 0);
  ASSERT_EQ(hist->wptr, 0);

  for (size_t i = 0; i < sz; i++) {
    ASSERT_FALSE(hist->data[i].addr);
    ASSERT_FALSE(hist->data[i].val);
    ASSERT_FALSE(hist->data[i].rw);
  }

  free(hist);
}

TEST_F(betsBusHistoryTest, AddShouldPushNewActivityToDataArray) {
  size_t end = hist->sz - 1;

  for (size_t i = 0; i < end; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
    ASSERT_EQ(hist->wptr, (i + 1));

    const struct betsBusActivity* act = &hist->data[i];
    ASSERT_EQ(hist->wptr, i + 1);
    ASSERT_EQ(act->addr, (uint16_t)i);
    ASSERT_EQ(act->val, (uint8_t)i);
    ASSERT_TRUE(act->rw);
  }

  ASSERT_EQ(hist->wptr, end);
  ASSERT_EQ(hist->rptr, 0);
}

TEST_F(betsBusHistoryTest, AddShouldPushReadPtrForwardIfFullCycle) {
  hist->sz = 5;
  hist->rptr = 3;
  hist->wptr = 2;

  betsBusHistory_add(hist, true, (uint16_t)0, (uint8_t)0);

  ASSERT_EQ(hist->wptr, 3);
  ASSERT_EQ(hist->rptr, 4);
}

TEST_F(betsBusHistoryTest, AddShouldWrapAroundIfArrayEndReached) {
  for (size_t i = 0; i < hist->sz; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
  }

  ASSERT_EQ(hist->wptr, 0);
  ASSERT_EQ(hist->rptr, 1);
}

TEST_F(betsBusHistoryTest, GetShouldPopActivity) {
  for (size_t i = 0; i < hist->sz - 1; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
  }

  while (hist->rptr != hist->wptr) {
    betsBusHistory_get(hist);
  }

  ASSERT_EQ(hist->wptr, hist->sz - 1);
  ASSERT_EQ(hist->wptr, hist->rptr);
}

TEST_F(betsBusHistoryTest, GetShouldReturnNullIfRWPtrsAreEqual) {
  const struct betsBusActivity* act;

  for (size_t i = 0; i < 3; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
  }

  for (size_t i = 0; i < 4; i++) {
    act = betsBusHistory_get(hist);
  }

  ASSERT_FALSE(act);
}

TEST_F(betsBusHistoryTest, ClearSouldWipeDataArrayButKeepingThePtrs) {
  for (size_t i = 0; i < hist->sz - 1; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
  }

  betsBusHistory_clear(hist);

  for (size_t i = 0; i < hist->sz; i++) {
    ASSERT_FALSE(hist->data[i].addr);
    ASSERT_FALSE(hist->data[i].val);
    ASSERT_FALSE(hist->data[i].rw);

  }

  ASSERT_EQ(hist->wptr, hist->sz - 1);
  ASSERT_FALSE(hist->rptr);
}

TEST_F(betsBusHistoryTest, ResetShouldWipeDataArrayAndSetPtrsToZero) {
  for (size_t i = 0; i < hist->sz - 1; i++) {
    betsBusHistory_add(hist, true, (uint16_t)i, (uint8_t)i);
  }

  for (size_t i = 0; i < hist->sz - 1; i++) {
    betsBusHistory_get(hist);
  }

  betsBusHistory_reset(hist);

  for (size_t i = 0; i < hist->sz; i++) {
    ASSERT_FALSE(hist->data[i].addr);
    ASSERT_FALSE(hist->data[i].val);
    ASSERT_FALSE(hist->data[i].rw);
  }

  ASSERT_FALSE(hist->wptr);
  ASSERT_FALSE(hist->rptr);
}