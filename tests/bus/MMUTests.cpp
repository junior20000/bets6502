#include <gtest/gtest.h>

extern "C" {
#include <bets6502/bus/mmu.h>
};

class betsMMUTest : public ::testing::Test {
 public:
  struct betsMMU* mmu;
  struct betsRegion regions[8] = {
      {{0, 0}, "gpu", (void*)"gpu", nullptr, nullptr},
      {{1, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{56, 57}, "rm2", (void*)"rm2", nullptr, nullptr},
      {{58, 58}, "sng", (void*)"sng", nullptr, nullptr}};

 protected:
  void SetUp() override {
    mmu = betsMMU_new(8);
    betsMMU_build(mmu, regions, 0, mmu->capacity - 1);
  }

  void TearDown() override { free(mmu); }
};

TEST(betsMMUCoreTest,
     NewShouldAllocateStructAndArrayBasedOnCapacityAndSetSizeToZero) {
  uint8_t cap = 10;
  struct betsMMU* mmu = betsMMU_new(cap);

  ASSERT_EQ(mmu->capacity, cap);
  ASSERT_EQ(mmu->size, 0);
  ASSERT_TRUE(mmu->regions);

  free(mmu);
}

TEST(betsMMUCoreDeathTest, InitShouldDieIfCapacityIsZero) {
  ASSERT_DEATH(betsMMU_new(0), "");
}

TEST(betsMMUCoreTest, RegisterRegionShouldPushBack) {
  uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[5] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr}};

  for (size_t i = 0; i < mmu->capacity; i++) {
    betsMMU_register_region(mmu, regions[i].mint.begin, regions[i].mint.end,
                            regions[i].id, regions[i].obj, regions[i].io_rd,
                            regions[i].io_wr);
    ASSERT_EQ(regions[i].mint.begin, mmu->regions[i].mint.begin);
    ASSERT_EQ(regions[i].mint.end, mmu->regions[i].mint.end);
    ASSERT_EQ(i + 1, mmu->size);
  }

  free(mmu);
}
TEST(betsMMUCoreTest, SegmentIsSortedShouldReturnTrueIfSortedWithFullCapacity) {
  uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[5] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr}};

  memcpy_s(&mmu->regions, cap * sizeof(struct betsRegion), &regions,
           sizeof(regions));
  mmu->size = 5;

  ASSERT_TRUE(betsMMU_regions_is_sorted(mmu));

  free(mmu);
}

TEST(betsMMUCoreTest, SegmentIsSortedShouldReturnTrueIfSortedWithHalfCapacity) {
  uint8_t cap = 6;
  struct betsMMU* const mmu = betsMMU_new(cap);

  betsMMU_register_region(mmu, 0, 10, "cpu", (void*)"cpu", nullptr, nullptr);
  betsMMU_register_region(mmu, 11, 21, "ram", (void*)"ram", nullptr, nullptr);
  betsMMU_register_region(mmu, 22, 32, "apu", (void*)"apu", nullptr, nullptr);

  ASSERT_TRUE(betsMMU_regions_is_sorted(mmu));

  free(mmu);
}

TEST(betsMMUCoreTest,
     SegmentIsSortedShouldReturnTrueIfOneOrZeroElementsInside) {
  uint8_t cap = 1;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[1] = {
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
  };

  memcpy_s(&mmu->regions, cap * sizeof(struct betsRegion), &regions,
           sizeof(regions));
  mmu->size = 0;

  ASSERT_TRUE(betsMMU_regions_is_sorted(mmu));

  mmu->size = 1;
  ASSERT_TRUE(betsMMU_regions_is_sorted(mmu));

  free(mmu);
}

TEST(betsMMUCoreTest, SegmentIsSortedShouldReturnFalseIfUnsortedWithFullCapacity) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr}};

  memcpy_s(&mmu->regions, cap * sizeof(struct betsRegion), &regions,
           sizeof(regions));
  mmu->size = 5;

  ASSERT_FALSE(betsMMU_regions_is_sorted(mmu));

  free(mmu);
}

TEST(betsMMUCoreTest,
     SegmentIsSortedShouldReturnFalseIfUnsortedWithHalfCapacity) {
  uint8_t cap = 6;
  struct betsMMU* const mmu = betsMMU_new(cap);

  betsMMU_register_region(mmu, 0, 10, "cpu", (void*)"cpu", nullptr, nullptr);
  betsMMU_register_region(mmu, 22, 32, "apu", (void*)"apu", nullptr, nullptr);
  betsMMU_register_region(mmu, 11, 21, "ram", (void*)"ram", nullptr, nullptr);

  ASSERT_FALSE(betsMMU_regions_is_sorted(mmu));

  free(mmu);
}

TEST(betsMMUCoreTest, SortArrayCorrectly) {
  struct betsRegion regions[5] = {
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr}};

  betsMMU_sort_regions(&regions[0], 0, 4);

  ASSERT_STREQ(regions[0].id, "cpu");
  ASSERT_STREQ(regions[1].id, "ram");
  ASSERT_STREQ(regions[2].id, "apu");
  ASSERT_STREQ(regions[3].id, "crt");
  ASSERT_STREQ(regions[4].id, "rom");
}

TEST(betsMMUCoreTest, RegisterRegionSafeShouldPushBackAndSort) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[5] = {
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr}};

  for (size_t i = 0; i < mmu->capacity; i++) {
    betsMMU_register_region_s(mmu, regions[i].mint.begin, regions[i].mint.end,
                              regions[i].id, regions[i].obj, regions[i].io_rd,
                              regions[i].io_wr);
    ASSERT_EQ(i + 1, mmu->size);
  }

  betsMMU_sort_regions(&regions[0], 0, 4);

  for (size_t i = 0; i < mmu->capacity; i++) {
    ASSERT_EQ(regions[i].mint.begin, mmu->regions[i].mint.begin);
    ASSERT_EQ(regions[i].mint.end, mmu->regions[i].mint.end);
  }

  free(mmu);
}

TEST(betsMMUCoreDeathTest, RegisterRegionSafeShouldDieIfIntervalsOverlaps) {
  const uint8_t cap = 6;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[6] = {
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{10, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{11, 22}, "rm2", (void*)"rm2", nullptr, nullptr}};

  for (size_t i = 0; i < mmu->capacity - 2; i++) {
    betsMMU_register_region_s(mmu, regions[i].mint.begin, regions[i].mint.end,
                              regions[i].id, regions[i].obj, regions[i].io_rd,
                              regions[i].io_wr);
    ASSERT_EQ(i + 1, mmu->size);
  }

  // Death with begin overlap
  ASSERT_DEATH(
      betsMMU_register_region_s(mmu, regions[4].mint.begin, regions[4].mint.end,
                                regions[4].id, regions[4].obj, regions[4].io_rd,
                                regions[4].io_wr),
      "");
  // Death with end overlap
  ASSERT_DEATH(
      betsMMU_register_region_s(mmu, regions[5].mint.begin, regions[5].mint.end,
                                regions[5].id, regions[5].obj, regions[5].io_rd,
                                regions[5].io_wr),
      "");

  free(mmu);
}

TEST(betsMMUCoreTest,
     BuildShouldCopyRegionsToInternalArrayAndComputeSizeCorrectly) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr}};

  ASSERT_EQ(mmu->capacity, 5);

  betsMMU_build(mmu, regions, 0, 4);
  ASSERT_TRUE(mmu->regions);
  ASSERT_EQ(mmu->capacity, mmu->size);

  for (size_t i = 0; i < mmu->capacity; i++) {
    ASSERT_EQ(mmu->regions[i].mint.begin, regions[i].mint.begin);
    ASSERT_EQ(mmu->regions[i].mint.end, regions[i].mint.end);
  }

  free(mmu);
};

TEST(betsMMUCoreDeathTest, BuildShouldDieIfInvalidArgs) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr}};

  // If MMU is NULL
  ASSERT_DEATH(betsMMU_build(nullptr, regions, 0, 4), "");
  // If array is NULL
  ASSERT_DEATH(betsMMU_build(mmu, nullptr, 0, 4), "");

  // If capacity is 0
  *(size_t*)&mmu->capacity = 0;
  ASSERT_DEATH(betsMMU_build(mmu, &regions[0], 0, 4), "");

  *(size_t*)&mmu->capacity = cap;

  // If start > end
  ASSERT_DEATH(betsMMU_build(mmu, &regions[0], 4, 0), "");

  // If the array of regions overwhelms the MMU capacity
  ASSERT_DEATH(betsMMU_build(mmu, &regions[0], 0, 10), "");

  free(mmu);
};

TEST(betsMMUCoreTest, BuildSortedShouldDoTheSameAsBuildButSorted) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr}};

  betsMMU_build_sorted(mmu, regions, 0, 4);
  ;
  ASSERT_EQ(mmu->capacity, mmu->size);

  betsMMU_sort_regions(&regions[0], 0, mmu->capacity - 1);

  for (size_t i = 0; i < mmu->capacity; i++) {
    ASSERT_EQ(mmu->regions[i].mint.begin, regions[i].mint.begin);
    ASSERT_EQ(mmu->regions[i].mint.end, regions[i].mint.end);
  }

  free(mmu);
};

TEST(betsMMUCoreTest, BuildSafeShouldBuildSortAndCheckForOverlaps) {
  const uint8_t cap = 5;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
  };

  betsMMU_build_s(mmu, regions, 0, 4);
  ASSERT_EQ(mmu->capacity, mmu->size);

  betsMMU_sort_regions(&regions[0], 0, mmu->capacity - 1);

  for (size_t i = 0; i < mmu->capacity; i++) {
    ASSERT_EQ(mmu->regions[i].mint.begin, regions[i].mint.begin);
    ASSERT_EQ(mmu->regions[i].mint.end, regions[i].mint.end);
  }

  free(mmu);
};

TEST(betsMMUCoreDeathTest, BuildSafeShouldDieIfOverlaps) {
  const uint8_t cap = 6;
  struct betsMMU* const mmu = betsMMU_new(cap);

  struct betsRegion regions[cap] = {
      {{0, 10}, "cpu", (void*)"cpu", nullptr, nullptr},
      {{22, 32}, "apu", (void*)"apu", nullptr, nullptr},
      {{33, 43}, "crt", (void*)"crt", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr},
      {{44, 54}, "rom", (void*)"rom", nullptr, nullptr},
      {{11, 21}, "ram", (void*)"ram", nullptr, nullptr}};

  ASSERT_DEATH(betsMMU_build_s(mmu, regions, 0, mmu->capacity - 1), "");

  free(mmu);
};

TEST_F(betsMMUTest, GetRegionReturnsRegionAtGivenInterval) {
  for each (struct betsRegion region in regions) {
    for (int j = region.mint.begin; j <= region.mint.end; j++) {
      ASSERT_STREQ(betsMMU_get_region_at(mmu, j)->id, region.id);
    }
  }
};

TEST_F(betsMMUTest, GetObjReturnsObjAtGivenInterval) {
  for each (struct betsRegion region in regions) {
    for (int j = region.mint.begin; j <= region.mint.end; j++) {
      auto id = (const char*)betsMMU_get_obj_at(mmu, j);
      ASSERT_STREQ(id, region.id);
    }
  }
};

TEST_F(betsMMUTest, GetObjReturnsNULLIfObjNotFound) {
  auto id = (const char*)betsMMU_get_obj_at(mmu, 55);
  ASSERT_FALSE(id);
};