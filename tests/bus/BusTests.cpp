#include <gtest/gtest.h>

extern "C" {
#include <bets6502/bus.h>
}

static void bus_reg1_write(void* buf, uint16_t addr, uint8_t data) {
  ((uint8_t*)buf)[addr] = data;
}

static void bus_reg2_write(void* buf, uint16_t addr, uint8_t data) {
  ((uint8_t*)buf)[addr] = data + 2;
}

static void bus_reg3_write(void* buf, uint16_t addr, uint8_t data) {
  ((uint8_t*)buf)[addr] = data + 3;
}

static uint8_t bus_reg1_read(void* buf, uint16_t addr) {
  return ((uint8_t*)buf)[addr];
}

class betsBusTest : public ::testing::Test {
 public:
  Bus_t bus;

 protected:
  void SetUp() override { betsBus_init(&bus, 256, 3, 0xFFFF); }

  void TearDown() override { betsBus_free(&bus); }
};

TEST(betsBusCoreTests, InitShouldAllocMemoryAndInitTheInternalMMU) {
  Bus_t bus;

  betsBus_init(&bus, 10, 1, 0xFFFF);

  ASSERT_TRUE(bus.mem);
  ASSERT_EQ(bus.mmu->capacity, 1);

  betsBus_free(&bus);
};

TEST(betsBusCoreTests, Init1ShouldAssignMemoryPtrAndInitTheMMU) {
  Bus_t bus;
  struct betsMemory* mem = betsMemory_new(10);

  betsBus_init1(&bus, 1, 0xFFFF, mem);

  ASSERT_EQ(bus.mem, mem);
  ASSERT_EQ(bus.mmu->capacity, 1);

  betsBus_free(&bus);
};

TEST_F(betsBusTest, WriteShouldCallTheWriteFunctionFromTheRegion) {
  auto wr1 = (iofn_wr)&bus_reg1_write;
  auto wr2 = (iofn_wr)&bus_reg2_write;
  auto wr3 = (iofn_wr)&bus_reg3_write;

  betsMMU_register_region_s(bus.mmu, 0, 9, "R1", bus.mem->data, nullptr,
                            wr1);
  betsMMU_register_region_s(bus.mmu, 10, 19, "R2", bus.mem->data, nullptr,
                            wr2);
  betsMMU_register_region_s(bus.mmu, 20, 252, "R3", bus.mem->data,
                            nullptr, wr3);

  for (uint8_t i = 0; i < 10; i++) {
    betsBus_write(&bus, i, i);
    ASSERT_EQ(bus.mem->data[i], i);
  }

  for (uint8_t i = 10; i < 20; i++) {
    betsBus_write(&bus, i, i);
    ASSERT_EQ(bus.mem->data[i], i + 2);
  }

  for (uint8_t i = 20; i < 253; i++) {
    betsBus_write(&bus, i, i);
    ASSERT_EQ(bus.mem->data[i], i + 3);
  }
};

TEST_F(betsBusTest, ReadShouldCallTheReadFunctionFromTheRegion) {
  auto rd = (iofn_rd)&bus_reg1_read;

  betsMMU_register_region_s(bus.mmu, 0, 255, "R1", bus.mem->data, rd,
                            nullptr);

  for (uint8_t i = 0; i < (uint8_t)bus.mem->sz; i++) {
    bus.mem->data[i] = i;
    ASSERT_EQ(betsBus_read(&bus, i), i);
  }
};