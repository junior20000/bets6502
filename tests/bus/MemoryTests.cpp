#include <gtest/gtest.h>

extern "C" {
#include <bets6502/bus/memory.h>
};

TEST(betsMemoryTest, NewShouldReturnPtrToInitializedStruct) {
  size_t sz = 10;
  struct betsMemory* mem = betsMemory_new(sz);

  ASSERT_EQ(mem->sz, sz);
  ASSERT_TRUE(mem->data);

  ASSERT_TRUE(mem);
  free(mem);
};

TEST(betsMemoryTest, ClearShouldSetDataToZero) {
  struct betsMemory* mem = nullptr;
  size_t sz = 10;

  mem = betsMemory_new(sz);

  for (uint8_t i = 1; i <= mem->sz; i++) {
    mem->data[i - 1] = i;
  }

  for (uint8_t i = 0; i < mem->sz; i++) {
    ASSERT_TRUE(mem->data[i]);
  }

  betsMemory_clear(mem);

  for (uint8_t i = 0; i < mem->sz; i++) {
    ASSERT_FALSE(mem->data[i]);
  }

  free(mem);
};