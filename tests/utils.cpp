#include "utils.hpp"

#include <sstream>
#include <string>

inline char gs_s(uint8_t status, ProcessorStatus_t flag) {
  return (status & flag) ? '+' : '-';
}

static std::string format_status(uint8_t status) {
  std::ostringstream s;

  s << gs_s(status, kN) << " | " << gs_s(status, kV) << " | "
    << gs_s(status, kU) << " | " << gs_s(status, kB) << " | "
    << gs_s(status, kD) << " | " << gs_s(status, kI) << " | "
    << gs_s(status, kZ) << " | " << gs_s(status, kC) << std::endl;

  return s.str();
}

::testing::AssertionResult StatusEquals(const char* m_expr, const char* n_expr,
                                        uint8_t status1, uint8_t status2) {
  if (status1 == status2) {
    return ::testing::AssertionSuccess();
  } else {
    const char header[31] = "N | V | U | B | D | I | Z | C\n";
    return ::testing::AssertionFailure()
           << "Expected the equality of both statuses:\n\n"
           << "  " << m_expr << ":\n"
           << "    " << header << "    " << format_status(status1) << "\n  "
           << n_expr << ":\n"
           << "    " << header << "    " << format_status(status2);
  }
}