#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502LogicalOperationsTests : public bets6502Test {};

TEST_F(bets6502LogicalOperationsTests,
       ANDShouldPerformLogicalANDBetweenAandMemory) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0xFF;
  bets6502_and(&cpu);

  ASSERT_EQ(cpu.acc, 0x80);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);

  cpu.cycles = 0;
  cpu.acc = 0x7F;
  bets6502_and(&cpu);

  ASSERT_EQ(cpu.acc, 0);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502LogicalOperationsTests, ANDShouldUpdateNZAsAppropriate) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0xFF;
  bets6502_and(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  cpu.acc = 0x7F;
  bets6502_and(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502LogicalOperationsTests,
       EORShouldPerformXORBetweenAandMemory) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0xFF;
  bets6502_eor(&cpu);

  ASSERT_EQ(cpu.acc, 0x7F);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);

  cpu.cycles = 0;
  cpu.acc = 0x80;
  bets6502_eor(&cpu);

  ASSERT_EQ(cpu.acc, 0);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502LogicalOperationsTests, EORShouldUpdateNZAsAppropriate) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0x7F;
  bets6502_eor(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  cpu.acc = 0x80;
  bets6502_eor(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502LogicalOperationsTests,
       ORAShouldPerformLogicalORBetweenAandMemory) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0xFF;
  bets6502_ora(&cpu);

  ASSERT_EQ(cpu.acc, 0xFF);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);

  cpu.cycles = 0;
  cpu.acc = 0x7F;
  bets6502_ora(&cpu);

  ASSERT_EQ(cpu.acc, 0xFF);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502LogicalOperationsTests, ORAShouldUpdateNZAsAppropriate) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0x7F;
  bets6502_ora(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  cpu.acc = 0x00;
  betsBus_write(cpu.bus, 0x0000, 0x00);

  bets6502_ora(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502LogicalOperationsTests,
       BITShouldNotAffectAandMemory) {
  betsBus_write(cpu.bus, 0x0000, 0x80);

  cpu.acc = 0xFF;
  bets6502_bit(&cpu);

  ASSERT_EQ(cpu.acc, 0xFF);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502LogicalOperationsTests, BITShouldUpdateNVZAsAppropriate) {
  betsBus_write(cpu.bus, 0x0000, 0x01);
  cpu.acc = 0xFE;

  bets6502_bit(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  betsBus_write(cpu.bus, 0x0000, 0x40);
  cpu.acc = 0x80;

  bets6502_bit(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kV);

  betsBus_write(cpu.bus, 0x0000, 0x80);
  cpu.acc = 0xFF;

  bets6502_bit(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  cpu.acc = 0x7F;

  bets6502_bit(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kN | kU | kZ);
}