#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502ShiftTest : public bets6502Test {};

TEST_F(bets6502ShiftTest, ASLShouldLeftShiftTheContentsOfMemory) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_asl(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 20);
  ASSERT_EQ(cpu.mdr, 20);
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502ShiftTest, ASLShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_asl(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x80);

  bets6502_asl(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  betsBus_write(cpu.bus, 0, 0x40);

  bets6502_asl(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);
}

TEST_F(bets6502ShiftTest, ASLAShouldLeftShiftA) {
  cpu.acc = 10;

  bets6502_asl_a(&cpu);

  ASSERT_EQ(cpu.acc, 20);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502ShiftTest, ASLAShouldUpdateNZCAsAppropriate) {
  cpu.acc = 10;

  bets6502_asl_a(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.status = kU;

  cpu.acc = 0x80;

  bets6502_asl_a(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  cpu.acc = 0x40;

  bets6502_asl_a(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);
}

TEST_F(bets6502ShiftTest, LSRShouldRightShiftTheContentsOfMemory) {
  betsBus_write(cpu.bus, 0, 10);

  bets6502_lsr(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 5);
  ASSERT_EQ(cpu.cycles, 3);

  betsBus_write(cpu.bus, 0, 0x81);

  bets6502_lsr(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x40);

  betsBus_write(cpu.bus, 0, 0);

  bets6502_lsr(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0);
}

TEST_F(bets6502ShiftTest, LSRShouldUpdateNZCAsAppropriate) {
  cpu.status |= kN;

  betsBus_write(cpu.bus, 0, 0x80);

  bets6502_lsr(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  betsBus_write(cpu.bus, 0, 2);

  bets6502_lsr(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_lsr(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_lsr(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502ShiftTest, LSRAShouldRightShiftA) {
  cpu.acc = 10;

  bets6502_lsr_a(&cpu);

  ASSERT_EQ(cpu.acc, 5);
  ASSERT_EQ(cpu.cycles, 1);

  cpu.acc = 0x81;

  bets6502_lsr_a(&cpu);

  ASSERT_EQ(cpu.acc, 0x40);
}

TEST_F(bets6502ShiftTest, LSRAShouldUpdateNZCAsAppropriate) {
  cpu.status |= kN;
  cpu.acc = 0x80;

  bets6502_lsr_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.acc = 2;

  bets6502_lsr_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_lsr_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_lsr_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ);
}

TEST_F(bets6502ShiftTest, ROLShouldRotateLeftTheBitsOfMemory) {
  betsBus_write(cpu.bus, 0, 0x40);

  bets6502_rol(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
  ASSERT_EQ(cpu.cycles, 3);

  bets6502_rol(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0);

  bets6502_rol(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 1);
}

TEST_F(bets6502ShiftTest, ROLShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 0x40);

  bets6502_rol(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  bets6502_rol(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_rol(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502ShiftTest, ROLAShouldRotateABitsToTheLeft) {
  cpu.acc = 0x40;

  bets6502_rol_a(&cpu);

  ASSERT_EQ(cpu.acc, 0x80);
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_rol_a(&cpu);

  ASSERT_EQ(cpu.acc, 0);

  bets6502_rol_a(&cpu);

  ASSERT_EQ(cpu.acc, 1);
}

TEST_F(bets6502ShiftTest, ROLAShouldUpdateNZCAsAppropriate) {
  cpu.acc = 0x20;

  bets6502_rol_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_rol_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  bets6502_rol_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_rol_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502ShiftTest, RORShouldRotateRightTheBitsOfMemory) {
  betsBus_write(cpu.bus, 0, 2);

  bets6502_ror(&cpu);

  ASSERT_EQ(betsBus_read(cpu.bus, 0), 1);
  ASSERT_EQ(cpu.cycles, 3);

  bets6502_ror(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0);

  bets6502_ror(&cpu);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x80);
}

TEST_F(bets6502ShiftTest, RORShouldUpdateNZCAsAppropriate) {
  betsBus_write(cpu.bus, 0, 2);

  bets6502_ror(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_ror(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_ror(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  bets6502_ror(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502ShiftTest, RORAShouldRotateABitsToTheRight) {
  cpu.acc = 2;

  bets6502_ror_a(&cpu);

  ASSERT_EQ(cpu.acc, 1);
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_ror_a(&cpu);
  ASSERT_EQ(cpu.acc, 0);

  bets6502_ror_a(&cpu);
  ASSERT_EQ(cpu.acc, 0x80);
}

TEST_F(bets6502ShiftTest, RORAShouldUpdateNZCAsAppropriate) {
  cpu.acc = 2;

  bets6502_ror_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_ror_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_ror_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kN | kU);

  bets6502_ror_a(&cpu);
  ASSERT_STATUS_EQ(cpu.status, kU);
}