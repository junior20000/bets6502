#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502ArithmeticTests : public bets6502Test {};

TEST_F(bets6502ArithmeticTests, ADCShouldSumTwoValuesTogheter) {
  betsBus_write(cpu.bus, 0, 10);

  cpu.acc = 10;

  bets6502_adc(&cpu);

  ASSERT_EQ(cpu.acc, 20);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502ArithmeticTests, ADCShouldUpdateCZVNAsAppropriate) {
  betsBus_write(cpu.bus, 0, 10);

  cpu.acc = 10;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  betsBus_write(cpu.bus, 0, 0xFF);
  cpu.acc = 1;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  bets6502_set_status(&cpu, kC, false);
  betsBus_write(cpu.bus, 0, 0x7F);
  cpu.acc = 1;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kV | kU);
}

#ifdef BETS6502_WITH_DECIMAL_MODE

TEST_F(bets6502ArithmeticTests,
       ADCWithBCDShouldSumTwoHexValuesTogheterAsDecimals) {
  bets6502_set_status(&cpu, kD, true);

  betsBus_write(cpu.bus, 0, 0x21);
  cpu.acc = 0x9;

  bets6502_adc(&cpu);

  ASSERT_EQ(cpu.acc, 0x30);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 0x21);
  ASSERT_EQ(cpu.cycles, 1);

  betsBus_write(cpu.bus, 0, 0x99);
  cpu.acc = 0x1;

  bets6502_adc(&cpu);

  ASSERT_EQ(cpu.acc, 0);
}

TEST_F(bets6502ArithmeticTests,
       ADCWithBCDShouldGiveTheCorrectOutputOfTwoInvalidNumbersWhenSummed) {
  bets6502_set_status(&cpu, kD, true);

  betsBus_write(cpu.bus, 0, 0x9B);
  cpu.acc = 0xBF;

  bets6502_adc(&cpu);

  ASSERT_EQ(cpu.acc, 0xB0);

  betsBus_write(cpu.bus, 0, 0x0B);
  cpu.acc = 0x0F;

  cpu.status = kU | kD;

  bets6502_adc(&cpu);

  ASSERT_EQ(cpu.acc, 0x10);
}

TEST_F(bets6502ArithmeticTests, ADCWithBCDShouldUpdateCZVNAsAppropriate) {
  bets6502_set_status(&cpu, kD, true);

  betsBus_write(cpu.bus, 0, 0x21);
  cpu.acc = 0x9;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD);

  betsBus_write(cpu.bus, 0, 0x99);
  cpu.acc = 0x1;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kD | kC);

  bets6502_set_status(&cpu, kC, false);
  betsBus_write(cpu.bus, 0, 0xFF);
  cpu.acc = 0x1;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kZ | kC);

  bets6502_set_status(&cpu, kC, false);
  betsBus_write(cpu.bus, 0, 0x99);
  cpu.acc = 0x99;

  bets6502_adc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kV | kU | kD | kC);
}

#endif

TEST_F(bets6502ArithmeticTests, SBCShouldSubtractCorrectly) {
  bets6502_set_status(&cpu, kC, true);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0xA;

  bets6502_sbc(&cpu);

  ASSERT_EQ(cpu.acc, 9);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 1);
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kC, false);

  betsBus_write(cpu.bus, 0, 0x2);
  cpu.acc = 0xA;

  bets6502_sbc(&cpu);

  ASSERT_EQ(cpu.acc, 7);
}

TEST_F(bets6502ArithmeticTests, SBCShouldUpdateCZVNAsAppropriate) {
  bets6502_set_status(&cpu, kC, true);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0xA;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  betsBus_write(cpu.bus, 0, 0xA);
  cpu.acc = 0xA;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kC | kZ);

  betsBus_write(cpu.bus, 0, 1);
  cpu.acc = 0;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kN);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0x80;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kV | kU | kC);

  betsBus_write(cpu.bus, 0, 0x80);
  cpu.acc = 1;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kV | kU);
}

#ifdef BETS6502_WITH_DECIMAL_MODE

TEST_F(bets6502ArithmeticTests, SBCWithBCDShouldSubtractCorrectly) {
  bets6502_set_status(&cpu, kD, true);
  bets6502_set_status(&cpu, kC, true);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0x9;

  bets6502_sbc(&cpu);

  ASSERT_EQ(cpu.acc, 8);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 1);
  ASSERT_EQ(cpu.cycles, 1);

  bets6502_set_status(&cpu, kC, false);
  betsBus_write(cpu.bus, 0, 0x2);
  cpu.acc = 0x9;

  bets6502_sbc(&cpu);

  ASSERT_EQ(cpu.acc, 6);

  bets6502_set_status(&cpu, kC, true);
  betsBus_write(cpu.bus, 0, 0x2);
  cpu.acc = 0x10;

  bets6502_sbc(&cpu);

  ASSERT_EQ(cpu.acc, 8);
}

TEST_F(bets6502ArithmeticTests, SBCWithBCDShouldUpdateCZVNAsAppropriate) {
  bets6502_set_status(&cpu, kD, true);
  bets6502_set_status(&cpu, kC, true);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0x10;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kC);

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.acc = 0x10;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kC | kZ);

  betsBus_write(cpu.bus, 0, 1);
  cpu.acc = 0;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kD | kN);

  betsBus_write(cpu.bus, 0, 0x1);
  cpu.acc = 0x80;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kV | kU | kD | kC);

  betsBus_write(cpu.bus, 0, 0x80);
  cpu.acc = 1;

  bets6502_sbc(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kV | kU | kD);
}

#endif

TEST_F(bets6502ArithmeticTests, CMPShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 10);

  cpu.acc = 20;

  bets6502_cmp(&cpu);

  ASSERT_EQ(cpu.mdr, 10);
  ASSERT_EQ(cpu.acc, 20);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.acc = 0x90;

  bets6502_cmp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.acc = 0x10;

  bets6502_cmp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.acc = 0x9;

  bets6502_cmp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502ArithmeticTests, CPXShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 10);

  cpu.x = 20;

  bets6502_cpx(&cpu);

  ASSERT_EQ(cpu.mdr, 10);
  ASSERT_EQ(cpu.x, 20);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.x = 0x90;

  bets6502_cpx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.x = 0x10;

  bets6502_cpx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.x = 0x9;

  bets6502_cpx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502ArithmeticTests, CPYShouldWorkAsIntended) {
  betsBus_write(cpu.bus, 0, 10);

  cpu.y = 20;

  bets6502_cpy(&cpu);

  ASSERT_EQ(cpu.mdr, 10);
  ASSERT_EQ(cpu.y, 20);
  ASSERT_EQ(betsBus_read(cpu.bus, 0), 10);
  ASSERT_EQ(cpu.cycles, 1);
  ASSERT_STATUS_EQ(cpu.status, kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.y = 0x90;

  bets6502_cpy(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.y = 0x10;

  bets6502_cpy(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ | kC);

  cpu.status = kU;

  betsBus_write(cpu.bus, 0, 0x10);
  cpu.y = 0x9;

  bets6502_cpy(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}