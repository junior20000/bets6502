#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502RegisterTransfersTests : public bets6502Test {};

TEST_F(bets6502RegisterTransfersTests, TAXShouldTransferAtoX) {
  cpu.acc = 10;
  cpu.x = 11;

  bets6502_tax(&cpu);

  ASSERT_EQ(cpu.x, 10);
  ASSERT_EQ(cpu.acc, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502RegisterTransfersTests, TAXShouldUpdateNZAsAppropriate) {
  cpu.acc = 10;
  cpu.x = 11;

  bets6502_tax(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.acc = 0;
  bets6502_tax(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.acc = 0x80;  // 7 bit is set.
  bets6502_tax(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502RegisterTransfersTests, TAYShouldTransferAtoY) {
  cpu.acc = 10;
  cpu.y = 11;

  bets6502_tay(&cpu);

  ASSERT_EQ(cpu.y, 10);
  ASSERT_EQ(cpu.acc, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502RegisterTransfersTests, TAYShouldUpdateNZAsAppropriate) {
  cpu.acc = 10;
  cpu.y = 11;

  bets6502_tay(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.acc = 0;
  bets6502_tay(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.acc = 0x80;  // 7 bit is set.
  bets6502_tay(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502RegisterTransfersTests, TXAShouldTransferXtoA) {
  cpu.acc = 10;
  cpu.x = 11;

  bets6502_txa(&cpu);

  ASSERT_EQ(cpu.x, 11);
  ASSERT_EQ(cpu.acc, 11);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502RegisterTransfersTests, TXAShouldUpdateNZAsAppropriate) {
  cpu.acc = 10;
  cpu.x = 11;

  bets6502_txa(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.x = 0;
  bets6502_txa(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.x = 0x80;  // 7 bit is set.
  bets6502_txa(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502RegisterTransfersTests, TYAShouldTransferYtoA) {
  cpu.acc = 10;
  cpu.y = 11;

  bets6502_tya(&cpu);

  ASSERT_EQ(cpu.y, 11);
  ASSERT_EQ(cpu.acc, 11);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502RegisterTransfersTests, TYAShouldUpdateNZAsAppropriate) {
  cpu.acc = 10;
  cpu.y = 11;

  bets6502_tya(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.y = 0;
  bets6502_tya(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.y = 0x80;  // 7 bit is set.
  bets6502_tya(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}