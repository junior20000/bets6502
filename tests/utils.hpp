#ifndef BETS6502TESTS_UTILS_H
#define BETS6502TESTS_UTILS_H

#include <gtest/gtest.h>
#include <stdint.h>

extern "C" {
#include <bets6502/6502.h>
}

#define ASSERT_STATUS_EQ(v1, v2) ASSERT_PRED_FORMAT2(StatusEquals, v1, v2)

static void memio_wr(void* buf, uint16_t addr, uint8_t data) {
  ((uint8_t*)buf)[addr] = data;
}

static uint8_t memio_rd(void* buf, uint16_t addr) {
  return ((uint8_t*)buf)[addr];
}

class bets6502Test : public ::testing::Test {
 public:
  bets6502 cpu;
  Bus_t bus;

  static constexpr size_t mem_sz = 0x10000;

  bets6502Test() {
    bets6502_init(&cpu);
    cpu.bus = &bus;
    betsBus_init(cpu.bus, mem_sz, 1, mem_sz);

    betsMMU_register_region(cpu.bus->mmu, 0, mem_sz - 1, "RAM", cpu.bus->mem->data,
                            &memio_rd, &memio_wr);
  }

 protected:
  void TearDown() override {
    bets6502_init(&cpu);
    betsMemory_clear(cpu.bus->mem);
  }
};

::testing::AssertionResult StatusEquals(const char* m_expr, const char* n_expr,
                                        uint8_t status1, uint8_t status2);

#endif  // !BETS6502TESTS_UTILS_H