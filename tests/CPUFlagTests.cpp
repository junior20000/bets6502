#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502FlagTest : public bets6502Test {};

template <typename F>
static void clear_set_test(bets6502* cpu, ProcessorStatus_t flag, bool is_set,
                           F func) {
  if (!is_set) {
    bets6502_set_status(cpu, flag, true);
  }

  func(cpu);

  if (is_set) {
    ASSERT_STATUS_EQ(cpu->status, kU | flag);
  } else {
    ASSERT_STATUS_EQ(cpu->status, kU);
  }

  ASSERT_EQ(cpu->cycles, 1);
}

TEST_F(bets6502FlagTest, CLCShouldClearCarry) {
  clear_set_test(&cpu, kC, false, &bets6502_clc);
}

TEST_F(bets6502FlagTest, CLDShouldClearDecimal) {
  clear_set_test(&cpu, kD, false, &bets6502_cld);
}

TEST_F(bets6502FlagTest, CLIShouldClearInterrupt) {
  clear_set_test(&cpu, kI, false, &bets6502_cli);
}

TEST_F(bets6502FlagTest, CLVShouldClearOverflow) {
  clear_set_test(&cpu, kV, false, &bets6502_clv);
}

TEST_F(bets6502FlagTest, SECShouldSetCarry) {
  clear_set_test(&cpu, kC, true, &bets6502_sec);
}

TEST_F(bets6502FlagTest, SEDShouldSetDecimal) {
  clear_set_test(&cpu, kD, true, &bets6502_sed);
}

TEST_F(bets6502FlagTest, SEIShouldSetInterrupt) {
  clear_set_test(&cpu, kI, true, &bets6502_sei);
}