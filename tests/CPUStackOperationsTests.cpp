#include <gtest/gtest.h>

#include "utils.hpp"

extern "C" {
#include <bets6502/6502.h>
}

class bets6502StackOperationsTests : public bets6502Test {};

TEST_F(bets6502StackOperationsTests, TSXShouldTransferSToX) {
  cpu.stkptr = 10;

  bets6502_tsx(&cpu);

  ASSERT_EQ(cpu.x, 10);
  ASSERT_EQ(cpu.stkptr, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502StackOperationsTests, TSXShouldUpdateNZAsAppropriate) {
  cpu.stkptr = 10;

  bets6502_tsx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.stkptr = 0;
  bets6502_tsx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  cpu.stkptr = 0x80;  // 7 bit is set.
  bets6502_tsx(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502StackOperationsTests, TXSShouldTransferSToX) {
  cpu.x = 10;

  bets6502_txs(&cpu);

  ASSERT_EQ(cpu.stkptr, 10);
  ASSERT_EQ(cpu.x, 10);
  ASSERT_EQ(cpu.cycles, 1);
}

TEST_F(bets6502StackOperationsTests, TXSShouldNotModifyP) {
  cpu.x = 10;

  bets6502_txs(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.x = 0;
  bets6502_txs(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  cpu.x = 0x80;  // 7 bit is set.
  bets6502_txs(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);
}

TEST_F(bets6502StackOperationsTests, PHAShouldPushAOnStack) {
  bets6502_reset(&cpu);
  cpu.cycles = 0;

  cpu.acc = 10;

  bets6502_pha(&cpu);

  ASSERT_EQ(cpu.stkptr, 0xFC);
  ASSERT_EQ(cpu.acc, 10);
  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_EQ(bets6502_spop(&cpu), 10);
  ASSERT_EQ(cpu.stkptr, 0xFD);
}

TEST_F(bets6502StackOperationsTests, PHPShouldPushPOnStack) {
  cpu.stkptr = 0xFD;
  cpu.status = kC | kN;
  bets6502_php(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kC | kN)
      << "Instruction should not change the values of P.";
  ASSERT_EQ(cpu.cycles, 2);
  ASSERT_STATUS_EQ(bets6502_spop(&cpu), kN | kU | kB | kC)
      << "Instruction should push Break and Unused with the P register.";
}

TEST_F(bets6502StackOperationsTests, PLAShouldPullAFromStack) {
  bets6502_reset(&cpu);

  cpu.acc = 10;

  bets6502_spush(&cpu, cpu.acc);
  cpu.cycles = 0;

  bets6502_pla(&cpu);

  ASSERT_EQ(cpu.stkptr, 0xFD);
  ASSERT_EQ(cpu.acc, 10);
  ASSERT_EQ(cpu.cycles, 3);
}

TEST_F(bets6502StackOperationsTests, PLAShouldUpdateNZAsAppropriate) {
  bets6502_spush(&cpu, 10);

  bets6502_pla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU);

  bets6502_spush(&cpu, 0);
  bets6502_pla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kU | kZ);

  bets6502_spush(&cpu, 0x80);  // 7 bit is set.
  bets6502_pla(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kU);
}

TEST_F(bets6502StackOperationsTests, PLPShouldPullPFromStack) {
  bets6502_reset(&cpu);

  cpu.status |= kC;
  cpu.status |= kB;
  cpu.status |= kN;

  bets6502_spush(&cpu, cpu.status);
  cpu.cycles = 0;

  bets6502_plp(&cpu);

  ASSERT_STATUS_EQ(cpu.status, kN | kI | kC);
  ASSERT_EQ(cpu.cycles, 3);
}