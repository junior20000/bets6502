<div align="center">
  <img src="docs/assets/banner.png" alt="logo" width="auto" height="auto" />
  <h1>bets6502</h1>
  <p>
    A cycle-accurate 6502 microprocessor emulator as a library!
  </p>
  <p>
    <a>
    <img src="https://img.shields.io/badge/99-00599C?style=flat&logo=c&logoColor=white" alt="C99">
    </a>
    <a href="https://cmake.org/cmake/help/latest/release/3.14.html">
      <img src="https://img.shields.io/badge/3.14-red?style=flat&logo=cmake&label=CMake">
    </a>
    <a href="https://gitlab.com/junior20000/bets6502/-/commits/master">
    <img src="https://img.shields.io/gitlab/last-commit/junior20000/bets6502" alt="last updated"/>
    </a>
    <a href="https://gitlab.com/junior20000/bets6502/-/blob/master/LICENSE">
    <img src="https://img.shields.io/gitlab/license/junior20000/bets6502" alt="license"/>
    </a>
  </p>
</div>

## Features

- Cycle-accurate emulation. Implemented in a [instruction-stepped and cycle-ticked](https://floooh.github.io/2019/12/13/cycle-stepped-6502.html#instruction-stepped-and-cycle-ticked) fashion.
- All valid opcodes and addressing modes implemented.
- Built-in [System bus](https://en.wikipedia.org/wiki/System_bus) and [MMU](https://en.wikipedia.org/wiki/Memory_management_unit).
- Optional decimal mode.
- Integration support with other devices in the bus, including interrupt requests.
- Edge cases and quirks are implemented.
- Unofficial opcodes (stable ones).

## Tests

### Unit tests:

More than 100 unit tests to ensure the correct functionalty!

- All addressing modes are covered and validated ✔️
- All valid opcodes are covered and validated ✔️
- Interrupts (hardware and software) are covered and validated, including edge cases ✔️

### [Klaus test suite:](https://github.com/Klaus2m5/6502_65C02_functional_tests)

- All valid opcodes and adressing modes are validated ✔️
- Hardware and software interrupts are validated ✔️
- BCD arithmetic for both intended and unintended decimal modes is validated ✔️

### Other test programs:

- [BigEd's timing test](https://github.com/BigEd/6502timing) validated all valid opcodes timings ✔️

## Contributing

This project is not open for contribution.

## Author

- [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/)
