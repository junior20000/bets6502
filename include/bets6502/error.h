#ifndef BETS6502_ERROR_H
#define BETS6502_ERROR_H

typedef enum bets6502ErrorCode bets6502ErrorCode;
typedef struct bets6502Error bets6502Error;

typedef void (*bets6502ErrorCallback)(bets6502ErrorCode code, const char* desc);

enum bets6502ErrorCode {
  BETS6502_NO_ERROR = 0,
  BETS6502_OUT_OF_MEMORY,
  BETS6502_NULL_POINTER,
  BETS6502_INVALID_SIZE,
  BETS6502_INVALID_MEMORY_RANGE,
  BETS6502_REGION_OVERLAP,
  BETS6502_NUMBER_OF_ERRORS
};

struct bets6502Error {
  bets6502ErrorCode err;
  const char* desc;
  bets6502ErrorCallback cb;
};

bets6502ErrorCallback bets6502Error_get_callback(void);
void bets6502Error_set_callback(bets6502ErrorCallback cb);

bets6502ErrorCode bets6502_get_error(void);
const char* bets6502_get_error_desc(void);
void bets6502_set_error(bets6502ErrorCode code, const char* desc);
void bets6502_clear_error(void);

#endif  // !BETS6502_ERROR_H