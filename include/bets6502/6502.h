#ifndef BETSNES_MICROPROCESSOR_6502_H
#define BETSNES_MICROPROCESSOR_6502_H

/************************************************************************/
/* Doxygen documentation                                                */
/************************************************************************/
/**
 * \file 6502.h
 * \brief The header of the 6502 API.
 */
/**
 * \defgroup cpu_registers Registers
 * \brief Registers reference for the 6502 microprocessor.
 *
 * This is the reference documentation for 6502 register related fields.
 */
/**
 * \defgroup cpu_mem_registers Memory Registers
 * \brief Memory registers are a minimal interface between between microprograms
 * and storage.
 *
 * \ingroup cpu_registers
 */
/**
 * \defgroup cpu_idx_registers Index Registers
 * \brief Index Registers reference for the 6502 microprocessor.
 *
 * This is the reference documentation for 6502 index register related fields.
 *
 * \ingroup cpu_registers
 */
/**
 * \defgroup cpu_int Interrupts
 * \brief Interrupts reference for the MOS 6502.
 */
/**
 * \defgroup cpu_addr_mode Addressing Modes
 * \brief Addressing modes reference for the 6502 microprocessor.
 *
 * This is the reference documentation for the addressing modes of the 6502
 * microprocessor.
 */
/**
 * \defgroup cpu_addr_mode_ninm Non-Indexed, Non Memory
 * \brief Non-Indexed, Non Memory addressing modes reference for the 6502
 * microprocessor.
 *
 * These addressing modes does not work with memory access.
 *
 * \ingroup cpu_addr_mode
 */
/**
 * \defgroup cpu_addr_mode_ni Non-Indexed
 * \brief Non-Indexed addressing modes reference for the 6502 microprocessor.
 *
 * These addressing modes works with memory access without the usage of the
 * index registers.
 *
 * \ingroup cpu_addr_mode
 */
/**
 * \defgroup cpu_addr_mode_idx Indexed
 * \brief Indexed addressing modes reference for the 6502 microprocessor.
 *
 * These addressing modes operate with memory access, using the index registers
 * as an offset to the address to be accessed.
 *
 * \ingroup cpu_addr_mode
 */
/**
 * \defgroup cpu_addr_mode_uni Unintended
 * \brief Addressing modes that are not present in any official documentation of
 * the MOS 6502.
 *
 * \ingroup cpu_addr_mode
 */
/**
 * \defgroup cpu_inst Instructions
 * \brief MOS 6502 instructions reference documentation.
 */
/**
 * \defgroup cpu_inst_ldst Load/Store Operations
 * \brief Instructions which performs I/O of a single byte between memory and
 * data registers (A, X, and Y).
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_rt Register Transfers
 * \brief Instructions that transfers data between the \ref cpu_idx_registers
 * "index registers" and the \ref bets6502.acc "accumulator".
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_stk Stack Operations
 * \brief Instructions that operates in conjunction with the stack.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_log Logical Operations
 * \brief Instructions that perform logical operations between the contents of
 * the accumulator and memory.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_am Arithmetic Operations
 * \brief Instructions that perform addition or subtraction on the contents of
 * A, X, or Y.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_incdec Increments & Decrements
 * \brief Instructions that increments or decrements the data from memory or
 * from the \ref cpu_idx_registers "Index Registers".
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_shf Shifts
 * \brief Instructions that shift, left or right, the contents of memory or
 * accumulator.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_jmp Jumps & Calls
 * \brief Instructions that modify PC, breaking the linear execution flow.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_bra Branches
 * \brief Instructions that modify PC by adding/subtracting an offset when
 * meeting a condition.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_flag Status Flag Changes
 * \brief Instructions that set or clear the C, D, I, or V flags.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_sys System Functions
 * \brief Instructions that affect the system.
 *
 * \ingroup cpu_inst
 */
/**
 * \defgroup cpu_inst_ill Illegal opcodes
 * \brief Instructions that are not present in any official documentation of the
 * MOS 6502.
 *
 * \ingroup cpu_inst
 */

#include <stdbool.h>
#include <stdint.h>

#include "bus.h"
#include "error.h"

/**
 * \brief Stack base memory address.
 */
#define BETS6502_STACK_BEGIN 0X0100

typedef struct Bus Bus_t;

/**
 * \brief The MOS 6502 microprocessor type.
 */
typedef struct bets6502 bets6502;

/**
 * \brief The tick logic of the processor ticks.
 *
 * \details This function is a callback for the \ref bets6502_tick function,
 * being called during the tick, before the \ref bets6502.cycles "cycle counter"
 * is incremented.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \sa bets6502_tick
 */
typedef void (*bets6502_tick_logic)(bets6502* mp);

/**
 * \brief MOS 6502 instruction type.
 *
 * \ingroup cpu_inst
 */
typedef struct bets6502Instruction bets6502Instruction;

/**
 * \brief Instruction set of the MOS 6502 microprocessor.
 *
 * \ingroup cpu_inst
 */
extern const bets6502Instruction bets6502_instset[256];

/**
 * \struct bets6502 6502.h <bets6502/6502.h>
 *
 * \brief The MOS 6502 microprocessor unit.
 */
typedef struct bets6502 {
  /**
   * \brief Program counter.
   * \details The program counter is responsible for holding the next
   * instruction to be fetched.
   *
   * \ingroup cpu_registers
   */
  uint16_t pc;
  /**
   * \brief Memory Address Register.
   *
   * \ingroup cpu_mem_registers
   */
  uint16_t mar;
  /**
   * \brief Memory Data Register.
   *
   * \ingroup cpu_mem_registers
   */
  uint8_t mdr;
  /**
   * \brief Instruction Register.
   * \details The instruction register stores the current instruction being
   * executed or decoded.
   *
   * \ingroup cpu_registers
   */
  uint8_t ir;
  uint8_t cycles;  ///< Elapsed cycles counter
  /**
   * \brief Status register.
   * \details Composed of eight one-bit registers (with one beign \ref kU
   * "unused"); Instructions may modify one or more bits, leaving the others
   * unchanged.
   *
   * \sa ProcessorStatus
   *
   * \ingroup cpu_registers
   */
  uint8_t status;
  /**
   * \brief The accumulator register.
   * \details Accumulators are one of the most important registers found in CPUs
   * and microprocessors.
   *
   * The accumulator is the only register capable of perform arithmetical
   * operations.
   *
   * \ingroup cpu_registers
   */
  uint8_t acc;
  /**
   * \brief The X index register.
   * \details The X index register is crucial register. Both \a X and \a Y
   * registers normally performs as counters or storing offsets for accessing
   * memory.
   *
   * \ingroup cpu_idx_registers
   */
  uint8_t x;
  /**
   * \brief The Y index register.
   * \details The Y index register is crucial register. Both \a X and \a Y
   * registers normally performs as counters or storing offsets for accessing
   * memory.
   *
   * \ingroup cpu_idx_registers
   */
  uint8_t y;
  /**
   * \brief Stack pointer
   * \details The stack pointer register holds the location of the first empty
   * place on the stack. The stack is used for temporary storage by machine
   * language programs, and by the computer.
   *
   * \ingroup cpu_registers
   */
  uint8_t stkptr;

  /**
   * \name Signals and States
   * \brief Members that denote if external devices or the user is triggering an
   * interrupt; Or if the processor is halted/jammed due to a call of the JAM
   * illegal opcode.
   *
   * \{
   */

  /**
   * \brief Signals if an entity requested an interrupt.
   *
   * False if no IRQ handling is pending, true otherwise.
   */
  bool is_irq_pending;

  /**
   * \brief Signals if an entity triggered an interrupt.
   *
   * False if no NMI handling is pending, true otherwise.
   */
  bool is_nmi_pending;

  /**
   * \brief Signals if an entity triggered a reset.
   *
   * False if no RES handling is pending, true otherwise.
   */
  bool is_res_pending;

  /**
   * \brief Signals if the microprocessor is jammed.
   */
  bool is_jammed;

  /** \} */  // End of Signals and States

  bets6502_tick_logic tick_logic;  ///< Tick logic callback.
  Bus_t* bus;                      ///< The system bus.
} bets6502;

/**
 * \brief CPU status flag type.
 *
 * CPU \ref bets6502.status "P register" mask values. Used to clear or set bits.
 *
 * \sa set_status
 * \sa get_status
 * \sa set_flag
 * \sa clear_flag
 */
typedef enum ProcessorStatus {
  kC = 1,         ///< Carry
  kZ = (1 << 1),  ///< Zero
  kI = (1 << 2),  ///< Interrupt Disable
  kD = (1 << 3),  ///< Decimal
  kB = (1 << 4),  ///< Break (B flag)
  kU = (1 << 5),  ///< Unused (always set to 1)
  kV = (1 << 6),  ///< Overflow
  kN = (1 << 7)   ///< Negative
} ProcessorStatus_t;

/**
 * \brief Instruction representation of the MOS 6502 microprocessor.
 *
 * \sa \ref bets6502_instset "Instruction set"
 *
 * \ingroup cpu_inst
 */
struct bets6502Instruction {
  /**
   * \brief Addressing mode logic.
   *
   * \sa cpu_addr_mode
   */
  void (*mode)(bets6502* mp);

  /**
   * \brief Instruction logic.
   *
   * \sa cpu_inst
   */
  void (*inst)(bets6502* mp);
};

/**
 * \name Constructor / Destructor
 * @{
 */
void bets6502_init(bets6502* mp);
void bets6502_free(bets6502* mp);
/** @} */  // End of Constructor / Destructor

/**
 * \name I/O
 * \brief Functions that start a \e READ or \e WRITE memory cycle.
 *
 * \{
 */

/**
 * \brief Read an U8 value from memory.
 *
 * Start a \e READ memory cycle from the bus.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The memory address to read from.
 *
 * \cycles_once
 *
 * \return The data read from the bus.
 *
 * \sa bets6502_readU16
 * \sa bets6502_readU16_s
 */
uint8_t bets6502_read(bets6502* mp, uint16_t addr);

/**
 * Read an U16 value from memory.
 *
 * Start two \e READ memory cycles from the bus from two addresses. The
 * effective address is formed by the logical disjunction of the data read from
 * \a adl and \a adh, which will be respectively the Address Low Byte and the
 * Address High Byte.
 *
 * \param[in,out] mp  The 6502 instance.
 * \param[in]     adl Address to read from which the data will be used as the
 * ADL.
 * \param[in]     adh Address to read from which the data will be used as
 * the ADH.
 *
 * \return The data read from the bus.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_read
 * \sa bets6502_readU16_s
 */
uint16_t bets6502_readU16(bets6502* mp, uint16_t adl, uint16_t adh);

/**
 * Read an U16 value sequentially from memory.
 *
 * Same as \ref bets6502_readU16, with \a addr being treated as \e adl and \addr
 * + 1 being treated as \e adh.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The base address to read from.
 *
 * \return The data read from the bus.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_read
 * \sa bets6502_readU16
 */
uint16_t bets6502_readU16_s(bets6502* mp, uint16_t addr);

/**
 * \brief Write an U8 value to memory.
 *
 * Start a \e WRITE memory cycle from the bus.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The memory address to write to.
 * \param[in]     data The data to be written.
 *
 * \cycles_once
 *
 * \sa bets6502_writeU16
 */
void bets6502_write(bets6502* mp, uint16_t addr, uint8_t data);

/**
 * \brief Write an U16 value sequentially to memory.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The memory address to write to.
 * \param[in]     data The data to be written.
 *
 * \cycles_once
 *
 * \sa bets6502_write
 */
void bets6502_writeU16(bets6502* mp, uint16_t addr, uint16_t data);

/** \} */  // End of I/O

/**
 * \name Stack memory operations
 * \brief Functions that start a \e READ or \e WRITE memory cycle, operating on
 * the Stack memory region.
 *
 * \{
 */

/**
 * \brief Push a \c byte to Stack.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     data The data to be pushed.
 *
 * \cycles_once
 *
 * \sa bets6502_spushU16
 */
void bets6502_spush(bets6502* mp, uint8_t data);

/**
 * \brief Push U16 value to Stack.
 *
 * Push the high-order byte and the low-order byte of \a data, respectively.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     data The data to be pushed.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_spush
 */
void bets6502_spushU16(bets6502* mp, uint16_t data);

/**
 * \brief Pop U8 value from Stack.
 *
 * Pop the U8 data stored in the memory location pointed by the stack pointer.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \return The data from Stack.
 *
 * \cycles_once
 *
 * \sa bets6502_spopU16
 */
uint8_t bets6502_spop(bets6502* mp);

/**
 * \brief Pop U16 value from Stack.
 *
 * Pop twice the U8 data stored in the memory locations pointed by the stack
 * pointer. The low-order byte is popped first, then the high-order byte.
 *
 * The logical disjunction of the former with the latter forms the data \c WORD.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_spopU16
 */
uint16_t bets6502_spopU16(bets6502* mp);

/** \} */  // End of Stack memory operations

/**
 * \name Cycle related
 * @{
 */
/**
 * \brief Tick the CPU once.
 *
 * \details The tick function calls the \ref bets6502VTbl.tick_logic callback
 * function if defined. Finally, the \ref bets6502.cycles "cycle counter" is
 * increased.
 *
 * \param[in] mp The 6502 instance to perform a cpu tick.
 *
 * \sa bets6502_tick_logic
 */
void bets6502_tick(bets6502* mp);
/** @} */  // End of Cycle related
/**
 * \name Interrupts
 * \{
 */
/**
 * \brief Reset the CPU.
 *
 * \details Reset the CPU \ref bets6502.status "P register" and \ref
 * bets6502.stkptr "stack pointer". Sets program counter to the address
 * contained inside the reset vector (0xFFFC, 0xFFFD).
 *
 * \todo Check if I do need to tick in the reset function.
 *
 * \param[in] mp The 6502 instance to perform a cpu tick.
 *
 * \cycles_fn{seven}
 *
 * \ingroup cpu_int
 */
void bets6502_reset(bets6502* mp);

/**
 * \brief Trigger a maskable interrupt request.
 *
 * \param[in,out] mp The 6502 instance to be interrupt.
 *
 * \cycles_fn{six}
 *
 * \sa bets6502_nmi
 *
 * \ingroup cpu_int
 */
void bets6502_irq(bets6502* mp);

/**
 * \brief Trigger a non-maskable interrupt request.
 *
 * \param[in,out] mp The 6502 instance to be interrupt.
 *
 * \cycles_fn{six}
 *
 * \sa bets6502_irq
 *
 * \ingroup cpu_int
 */
void bets6502_nmi(bets6502* mp);
/** \} */  // End of Interrupts

bool bets6502_get_status(const bets6502* mp, ProcessorStatus_t status);
void bets6502_set_status(bets6502* mp, ProcessorStatus_t status, bool state);

/************************************************************************/
/* Addressing modes                                                     */
/************************************************************************/

/**
 * \brief Accumulator addressing mode.
 *
 * \details This addressing mode has the \ref bets6502.acc "accumulator" as the
 * operand. Its ASM form is <tt> \<mnemonic\> A </tt>. In addition, the same is
 * used exclusively by \ref cpu_inst_shf "shift instructions".
 *
 * \note The Accumulator addressing mode does not add additional cycles itself.
 * No prior operations are necessary.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \no_cycles
 *
 * \ingroup cpu_addr_mode_ninm
 */
void bets6502_am_a(bets6502* mp);

/**
 * \brief Immediate addressing mode.
 *
 * \details This addressing mode has the second byte of the instruction as the
 * operand. Its ASM form is <tt> \<mnemonic\> #$xx </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \no_cycles
 *
 * \ingroup cpu_addr_mode_ninm
 */
void bets6502_am_imm(bets6502* mp);

/**
 * \brief Implied addressing mode.
 *
 * \details The operand is implicit in the opcode of the instruction. Its ASM
 * form is <tt> \<mnemonic\> </tt>.
 *
 * \note The Implied addressing mode does not add additional cycles itself.
 * Also, most of the implied instructions have a tick in the end, and not in the
 * beginning. No prior operations are necessary.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \no_cycles
 *
 * \ingroup cpu_addr_mode_ninm
 */
void bets6502_am_imp(bets6502* mp);

/**
 * \brief Relative addressing mode.
 *
 * \details This addressing mode is used exclusively by branch instructions; The
 * second byte is the branch offset, establishing the destination of the
 * conditional branch. Its ASM form is <tt> \<mnemonic\> $xx </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \no_cycles
 *
 * \ingroup cpu_addr_mode_ni
 */
void bets6502_am_rel(bets6502* mp);

/**
 * \brief Absolute addressing mode.
 *
 * \details This addressing mode uses the two following bytes of the instruction
 * as the absolute memory address to be accessed. The second byte represents the
 * eight low-order bits of the memory address, while the third byte is the eight
 * high-order bits, forming the effective 16-bit address operand. Its ASM form
 * is <tt> \<mnemonic\> $LLHH </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_fn{two}
 *
 * \ingroup cpu_addr_mode_ni
 */
void bets6502_am_abs(bets6502* mp);

/**
 * \brief Zero Page addressing mode.
 *
 * \details This addressing mode is only capable of doing memory accesses
 * within the zero page area, leading to a faster I/O as the CPU does not need
 * an extra cycle to fetch the high byte. Its ASM form is <tt> \<mnemonic\>
 * $xx </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_once
 *
 * \ingroup cpu_addr_mode_ni
 */
void bets6502_am_zpg(bets6502* mp);

/**
 * \brief Indirect addressing mode.
 *
 * \details The Indirect (also known as <b> Absolute Indirect </b>) addressing
 * mode uses pointers for data access; Its use is exclusive of the \ref
 * bets6502_jmp "JMP" instruction. Just as the Absolute addressing mode, the
 * second byte represents the eight low-order bits of the memory address, while
 * the third byte is the eight high-order bits, forming the effective 16-bit
 * address operand. Its ASM form is <tt> JMP ($LLHH) </tt>.
 *
 * For more information, please check the \ref bets6502_jmp documentation.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_fn{four}
 *
 * \ingroup cpu_addr_mode_ni
 */
void bets6502_am_ind(bets6502* mp);

/**
 * \brief X-Indexed Absolute addressing mode
 *
 * \details This addressing mode adds the content of the X register to the
 * absolute address, forming the effective address. Its ASM form is <tt>
 * \<mnemonic\> $LLHH, X </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b twice if no page-boundary is crossed, \b
 * thrice otherwise.
 *
 * \sa bets6502_am_abs
 * \sa bets6502_am_aby
 * \sa bets6502_am_abx_rmw
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_abx(bets6502* mp);

/**
 * \brief X-Indexed Absolute addressing mode
 *
 * \details Similar to the \ref bets6502_am_abx with the exception that it takes
 * an extra cycle due to a discarded data read at T3; No page is crossed.\n
 *
 * \note This variation of the ABS addressing mode is only used between
 * Read-Modify-Write instructions.
 *
 * \todo Check if I can tick from inside the instructions that has this
 * exception instead of creating a separate addressing mode.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_am_abs
 * \sa bets6502_am_aby
 * \sa bets6502_am_abx
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_abx_rmw(bets6502* mp);

/**
 * \brief Y-Indexed Absolute addressing mode
 *
 * \details This addressing mode adds the content of the Y register to the
 * absolute address, forming the effective address. Its ASM form is <tt>
 * \<mnemonic\> $LLHH, Y </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b twice if no page-boundary is crossed, \b
 * thrice otherwise.
 *
 * \sa bets6502_am_abs
 * \sa bets6502_am_abx
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_aby(bets6502* mp);

/**
 * \brief X-Indexed Zero Page addressing mode
 *
 * \details This addressing mode adds the content of the X register to the
 * second byte following the instruction, forming the effective address
 * contained inside the zero page. Its ASM form is <tt> \<mnemonic\> $xx, X
 * </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b twice. The Indexed Zero Page and the X-Indexed
 * Indirect are the only indexed addressing modes that will never cross a page
 * boundary.
 *
 * \sa bets6502_am_zpg
 * \sa bets6502_am_zpy
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_zpx(bets6502* mp);

/**
 * \brief Y-Indexed Zero Page addressing mode
 *
 * \details This addressing mode adds the content of the Y register to the
 * second byte following the instruction, forming the effective address
 * contained inside the zero page. Its ASM form is <tt> \<mnemonic\> $xx, Y
 * </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b twice. The Indexed Zero Page and the X-Indexed
 * Indirect are the only indexed addressing modes that will never cross a page
 * boundary.
 *
 * \sa bets6502_am_zpg
 * \sa bets6502_am_zpx
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_zpy(bets6502* mp);

/**
 * \brief X-Indexed Indirect addressing mode
 *
 * \details This addressing mode specify an address inside the zero page and
 * adds the content of the X register to it, forming the address of a pointer;
 * This pointer points to the low-order eight bits of the effective address and
 * the following address points to the high-order eight bits of the same. Its
 * ASM form is <tt> \<mnemonic\> ($xx, X) </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b four times. The Indexed Zero Page and the
 * X-Indexed Indirect are the only indexed addressing modes that will never
 * cross a page boundary.
 *
 * \sa bets6502_am_ind
 * \sa bets6502_am_idy
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_idx(bets6502* mp);

/**
 * \brief Y-Indexed Indirect addressing mode
 *
 * \details This addressing mode specify an address inside the zero page and
 * adds the content of the Y register to it, forming the address of a pointer;
 * This pointer points to the low-order eight bits of the effective address and
 * the following address points to the high-order eight bits of the same. Its
 * ASM form is <tt> \<mnemonic\> ($xx), Y </tt>.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles This function ticks \b four times if no page-boundary is crossed, \b
 * five otherwise.
 *
 * \sa bets6502_am_ind
 * \sa bets6502_am_idx
 *
 * \ingroup cpu_addr_mode_idx
 */
void bets6502_am_idy(bets6502* mp);

/************************************************************************/
/* Unintended addressing modes                                          */
/************************************************************************/

/**
 * \brief Y-Indexed Absolute addressing mode.
 *
 * Same as Absolute, X (RMW) addressing mode, taking an extra cycle due to a
 * data discard (read) at T3.
 *
 * \note The only official instruction
 * 
 * \remarks The equivalent legal mode is the X-Indexed Absolute (RMW).
 *
 * \cycles_fn(three)
 *
 * \sa bets6502_am_abx_rmw
 *
 * \ingroup cpu_addr_mode_uni
 */
void bets6502_am_aby_rmw(bets6502* mp);

/**
 * \brief Y-Indexed Indirect addressing mode
 *
 * Similar to Indirect, Y addressing mode, with the exception of always
 * triggering the extra tick, even if no page is crossed, due to the data
 * discrad (read) at T4.
 *
 * \remarks The equivalent legal mode is the Indirect, Y (RMW).
 * 
 * \cycles_fn{four}
 * 
 * \sa bets6502_am_idy
 *
 * \ingroup cpu_addr_mode_uni
 */
void bets6502_am_idy_rmw(bets6502* mp);

/************************************************************************/
/* Instructions                                                         */
/************************************************************************/

// Load/Store
/**
 * \brief Fetch data from memory into the accumulator. Zero and Negative flags
 * are updated upon the value loaded.
 *
 * \param[in,out] mp  The 6502 instance with the registers where the operation
 * will be performed.
 *
 * \cycles_once
 *
 * \sa bets6502_ldx
 * \sa bets6502_ldy
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_lda(bets6502* mp);

/**
 * \brief Load the value from memory into the x register. Zero and Negative
 * flags are updated upon the value loaded.
 *
 * \param[in,out] mp  The 6502 instance with the registers where the operation
 * will be performed.
 *
 * \cycles_once
 *
 * \sa bets6502_lda
 * \sa bets6502_ldy
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_ldx(bets6502* mp);

/**
 * \brief Load the value from memory into the y register. Zero and Negative
 * flags are updated upon the value loaded.
 *
 * \param[in,out] mp  The 6502 instance with the registers where the operation
 * will be performed.
 *
 * \cycles_once
 *
 * \sa bets6502_lda
 * \sa bets6502_ldx
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_ldy(bets6502* mp);

/**
 * \brief Store the data from the \ref bets6502.acc "accumulator" into memory.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_once
 *
 * \sa bets6502_stx
 * \sa bets6502_sty
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_sta(bets6502* mp);

/**
 * \brief Store the data from the \ref bets6502.x "X" register into memory.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_once
 *
 * \sa bets6502_sta
 * \sa bets6502_sty
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_stx(bets6502* mp);

/**
 * \brief Store the data from the \ref bets6502.y "Y" register into memory.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \cycles_once
 *
 * \sa bets6502_sta
 * \sa bets6502_stx
 *
 * \ingroup cpu_inst_ldst
 */
void bets6502_sty(bets6502* mp);

/**
 * \brief Transfer the data from A to X.
 *
 * \cycles_once
 *
 * \sa bets6502_tay
 * \sa bets6502_txa
 * \sa bets6502_tya
 *
 * \ingroup cpu_inst_rt
 */
void bets6502_tax(bets6502* mp);

/**
 * \brief Transfer the data from A to Y.
 *
 * \cycles_once
 *
 * \sa bets6502_tax
 * \sa bets6502_txa
 * \sa bets6502_tya
 *
 * \ingroup cpu_inst_rt
 */
void bets6502_tay(bets6502* mp);

/**
 * \brief Transfer the data from X to A.
 *
 * \cycles_once
 *
 * \sa bets6502_tax
 * \sa bets6502_tay
 * \sa bets6502_tya
 *
 * \ingroup cpu_inst_rt
 */
void bets6502_txa(bets6502* mp);

/**
 * \brief Transfer the data from Y to A.
 *
 * \cycles_once
 *
 * \sa bets6502_tax
 * \sa bets6502_tay
 * \sa bets6502_txa
 *
 * \ingroup cpu_inst_rt
 */
void bets6502_tya(bets6502* mp);

/**
 * \brief Transfer the S to X.
 *
 * \cycles_once
 *
 * \sa bets6502_txs
 * \sa bets6502_pha
 * \sa bets6502_php
 * \sa bets6502_pla
 * \sa bets6502_plp
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_tsx(bets6502* mp);

/**
 * \brief Transfer the X to S.
 *
 * \cycles_once
 *
 * \sa bets6502_tsx
 * \sa bets6502_pha
 * \sa bets6502_php
 * \sa bets6502_pla
 * \sa bets6502_plp
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_txs(bets6502* mp);

/**
 * \brief Push A on the stack.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_tsx
 * \sa bets6502_txs
 * \sa bets6502_php
 * \sa bets6502_pla
 * \sa bets6502_plp
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_pha(bets6502* mp);

/**
 * \brief Push P on the stack.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_tsx
 * \sa bets6502_txs
 * \sa bets6502_pha
 * \sa bets6502_pla
 * \sa bets6502_plp
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_php(bets6502* mp);

/**
 * \brief Pull A from the stack.
 *
 * \details Pulls the 8-bit data from the stack and assigns to A.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_tsx
 * \sa bets6502_txs
 * \sa bets6502_pha
 * \sa bets6502_php
 * \sa bets6502_plp
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_pla(bets6502* mp);

/**
 * \brief Pull S from the stack.
 *
 * \details Pulls the 8-bit data from the stack and assigns to P.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_tsx
 * \sa bets6502_txs
 * \sa bets6502_pha
 * \sa bets6502_php
 * \sa bets6502_pla
 *
 * \ingroup cpu_inst_stk
 */
void bets6502_plp(bets6502* mp);

/**
 * \brief Perform logical AND on A using the data the memory.
 *
 * \cycles_once
 *
 * \sa bets6502_eor
 * \sa bets6502_ora
 * \sa bets6502_bit
 *
 * \ingroup cpu_inst_log
 */
void bets6502_and(bets6502* mp);

/**
 * \brief Perform XOR on A using the data the memory.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_ora
 * \sa bets6502_bit
 *
 * \ingroup cpu_inst_log
 */
void bets6502_eor(bets6502* mp);

/**
 * \brief Perform logical OR on A using the data the memory.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_eor
 * \sa bets6502_bit
 *
 * \ingroup cpu_inst_log
 */
void bets6502_ora(bets6502* mp);

/**
 * \brief Test if one or more bits are set in the specified memory location.
 *
 * \details A is used as a mask to this operation, being ANDed with the value in
 * memory.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_eor
 * \sa bets6502_bit
 *
 * \ingroup cpu_inst_log
 */
void bets6502_bit(bets6502* mp);

/**
 * \brief Add with Carry
 *
 * \details Add the contents of memory to A in conjunction with the carry bit.
 *
 * \note Both ADC and SBC are the only instructions which the decimal mode flag
 * is used.
 *
 * \cycles_once
 *
 * \sa bets6502_sbc
 * \sa bets6502_cmp
 * \sa bets6502_cpx
 * \sa bets6502_cpy
 *
 * \ingroup cpu_inst_am
 */
void bets6502_adc(bets6502* mp);

/**
 * \brief Subtract with Carry
 *
 * \details Subtract the memory contents from A in conjunction with the NOT of
 * the carry bit.
 *
 * \note Both ADC and SBC are the only instructions which the decimal mode flag
 * is used.
 *
 * \cycles_once
 *
 * \sa bets6502_adc
 * \sa bets6502_cmp
 * \sa bets6502_cpx
 * \sa bets6502_cpy
 *
 * \ingroup cpu_inst_am
 */
void bets6502_sbc(bets6502* mp);

/**
 * \brief Compare A with memory.
 *
 * \cycles_once
 *
 * \sa bets6502_adc
 * \sa bets6502_sbc
 * \sa bets6502_cpx
 * \sa bets6502_cpy
 *
 * \ingroup cpu_inst_am
 */
void bets6502_cmp(bets6502* mp);

/**
 * \brief Compare X with memory.
 *
 * \cycles_once
 *
 * \sa bets6502_adc
 * \sa bets6502_sbc
 * \sa bets6502_cmp
 * \sa bets6502_cpy
 *
 * \ingroup cpu_inst_am
 */
void bets6502_cpx(bets6502* mp);

/**
 * \brief Compare Y with memory.
 *
 * \cycles_once
 *
 * \sa bets6502_adc
 * \sa bets6502_sbc
 * \sa bets6502_cmp
 * \sa bets6502_cpx
 *
 * \ingroup cpu_inst_am
 */
void bets6502_cpy(bets6502* mp);

/**
 * \brief Increment memory.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_inx
 * \sa bets6502_iny
 * \sa bets6502_dec
 * \sa bets6502_dex
 * \sa bets6502_dey
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_inc(bets6502* mp);

/**
 * \brief Increment X.
 *
 * \cycles_once
 *
 * \sa bets6502_inc
 * \sa bets6502_iny
 * \sa bets6502_dec
 * \sa bets6502_dex
 * \sa bets6502_dey
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_inx(bets6502* mp);

/**
 * \brief Increment Y.
 *
 * \cycles_once
 *
 * \sa bets6502_inc
 * \sa bets6502_inx
 * \sa bets6502_dec
 * \sa bets6502_dex
 * \sa bets6502_dey
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_iny(bets6502* mp);

/**
 * \brief Decrement memory.
 *
 * \cycles_fn{two}
 *
 * \sa bets6502_inc
 * \sa bets6502_inx
 * \sa bets6502_iny
 * \sa bets6502_dex
 * \sa bets6502_dey
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_dec(bets6502* mp);

/**
 * \brief Decrement X.
 *
 * \cycles_once
 *
 * \sa bets6502_inc
 * \sa bets6502_inx
 * \sa bets6502_iny
 * \sa bets6502_dec
 * \sa bets6502_dey
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_dex(bets6502* mp);

/**
 * \brief Decrement Y.
 *
 * \cycles_once
 *
 * \sa bets6502_inc
 * \sa bets6502_inx
 * \sa bets6502_iny
 * \sa bets6502_dec
 * \sa bets6502_dex
 *
 * \ingroup cpu_inst_incdec
 */
void bets6502_dey(bets6502* mp);

/**
 * \brief Arithmetic left shift.
 *
 * Shift the contents of memory one bit left.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_asl(bets6502* mp);

/**
 * \brief Arithmetic left shift on A.
 *
 * Shift A one bit left.
 *
 * \cycles_once
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_lsr_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_asl_a(bets6502* mp);

/**
 * \brief Logical right shift.
 *
 * Shift the contents of memory one bit right.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_asl
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_lsr(bets6502* mp);

/**
 * \brief Logical right shift on A.
 *
 * Shift A one bit right.
 *
 * \cycles_once
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_lsr_a(bets6502* mp);

/**
 * \brief Rotate left.
 *
 * Move the bits of memory one place to the left.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_rol(bets6502* mp);

/**
 * \brief Rotate left on A.
 *
 * Move A's bits one place to the left.
 *
 * \cycles_once
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_rol_a(bets6502* mp);

/**
 * \brief Rotate right.
 *
 * Move the bits of memory one place to the right.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_rol_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_ror(bets6502* mp);

/**
 * \brief Rotate right on A.
 *
 * Move A's bits one place to the right.
 *
 * \cycles_once
 *
 * \sa bets6502_asl
 * \sa bets6502_lsr
 * \sa bets6502_rol
 * \sa bets6502_ror
 * \sa bets6502_asl_a
 * \sa bets6502_lsr_a
 * \sa bets6502_ror_a
 *
 * \ingroup cpu_inst_shf
 */
void bets6502_ror_a(bets6502* mp);

/**
 * \brief Jump to another location in memory.
 *
 * Set PC to the address specified by the operand.
 *
 * \no_cycles
 *
 * \sa bets6502_jsr
 * \sa bets6502_rts
 *
 * \ingroup cpu_inst_jmp
 */
void bets6502_jmp(bets6502* mp);

/**
 * \brief Jump to subroutine.
 *
 * \details Push PC to the stack so you can return later.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_jmp
 * \sa bets6502_rts
 *
 * \ingroup cpu_inst_jmp
 */
void bets6502_jsr(bets6502* mp);

/**
 * \brief Return from subroutine.
 *
 * \details Set PC to the next location where JSR was called.
 *
 * \cycles_fn{five}
 *
 * \sa bets6502_jmp
 * \sa bets6502_jsr
 *
 * \ingroup cpu_inst_jmp
 */
void bets6502_rts(bets6502* mp);

/**
 * \brief Branch if Carry clear.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bcs
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bcc(bets6502* mp);

/**
 * \brief Branch if Carry set.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bcs
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bcs(bets6502* mp);

/**
 * \brief Branch if equal.
 *
 * Branch if Zero is set.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bne
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_beq(bets6502* mp);

/**
 * \brief Branch if minus.
 *
 * Branch if Negative is set.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bpl
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bmi(bets6502* mp);

/**
 * \brief Branch if not equal.
 *
 * Branch if Zero is clear.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_beq
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bne(bets6502* mp);

/**
 * \brief Branch if positive.
 *
 * Branch if Negative is clear.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bmi
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bpl(bets6502* mp);

/**
 * \brief Branch if Overflow clear.
 *
 * Branch if Overflow is clear.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bvs
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bvc(bets6502* mp);

/**
 * \brief Branch if Overflow set.
 *
 * Branch if Overflow is set.
 *
 * \cycles This function ticks \b once if conditions do not meet, \b twice
 * without crossing a page boundary, and \b thrice otherwise.
 *
 * \sa bets6502_bvs
 *
 * \ingroup cpu_inst_bra
 */
void bets6502_bvs(bets6502* mp);

/**
 * \brief Clear Carry
 *
 * \cycles_once
 *
 * \sa bets6502_sec
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_clc(bets6502* mp);

/**
 * \brief Clear Decimal
 *
 * \cycles_once
 *
 * \sa bets6502_sed
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_cld(bets6502* mp);

/**
 * \brief Clear Interrupt
 *
 * \cycles_once
 *
 * \sa bets6502_sei
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_cli(bets6502* mp);

/**
 * \brief Clear Overflow
 *
 * \cycles_once
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_clv(bets6502* mp);

/**
 * \brief Set Carry
 *
 * \cycles_once
 *
 * \sa bets6502_clc
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_sec(bets6502* mp);

/**
 * \brief Set Decimal
 *
 * \cycles_once
 *
 * \sa bets6502_cld
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_sed(bets6502* mp);

/**
 * \brief Set Interrupt
 *
 * \cycles_once
 *
 * \sa bets6502_cli
 *
 * \ingroup cpu_inst_flag
 */
void bets6502_sei(bets6502* mp);

/**
 * \brief Force interrupt.
 *
 * Force an IRQ (interrupt request).
 *
 * \note This operation may suffer an interrupt hijack during the first four
 * cycles (counting with the fetch�decode cycle).
 *
 * \remarks Although the BRK instruction does in fact generates an \ref
 * bets6502_irq "interrupt request", it does still a callable instruction from
 * code.
 *
 * \cycles_fn{six}
 *
 * \sa cpu_int
 *
 * \ingroup cpu_inst_sys
 */
void bets6502_brk(bets6502* mp);

/**
 * \brief No operation.
 *
 * Does nothing / skip to next instruction. \emoji :neutral_face:
 *
 * \remark Although the BRK instruction does in fact generates an \ref
 * bets6502_irq "interrupt request", it does still a callable instruction from
 * code.
 *
 * \cycles_once
 *
 * \ingroup cpu_inst_sys
 */
void bets6502_nop(bets6502* mp);

/**
 * \brief Return from interrupt.
 *
 * \cycles_fn{five}
 *
 * \sa cpu_int
 *
 * \ingroup cpu_inst_sys
 */
void bets6502_rti(bets6502* mp);

/************************************************************************/
/* Illegal opcodes                                                      */
/************************************************************************/

/**
 * \name Stable opcodes
 * \brief Illegal opcodes that are completely stable, i.e., opcodes that do not
 * need special precautions.
 *
 * \{
 */

/**
 * \brief Perform logical AND on A using immediate data and then LSRs the
 * result.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e AND and \e LSR.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_lsr
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_alr(bets6502* mp);

/**
 * \brief Perform logical AND on A using immediate data.
 *
 * \details This instruction is similar to \ref bets6502_and, with the only
 * difference being that the N flag is copied to Carry.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e AND and \e ASL or \e ROL.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_asl
 * \sa bets6502_rol
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_anc(bets6502* mp);

/**
 * \brief Perform logical AND on A using immediate data; RORs the result.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e AND and \e ROR.
 *
 * \cycles_once
 *
 * \sa bets6502_and
 * \sa bets6502_ror
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_arr(bets6502* mp);

/**
 * \brief Decrement the contents of memory and compare the result with A.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e DEC and \e CMP.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_dec
 * \sa bets6502_cmp
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_dcp(bets6502* mp);

/**
 * \brief Increment the contents of memory; Subtract the result from A.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \note This instruction inherits the decimal flag dependency from SBC.
 *
 * \remark This instruction has as sub-instructions \e INC and \e SBC.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_inc
 * \sa bets6502_sbc
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_isc(bets6502* mp);

/**
 * \brief Processor lock-up.
 *
 * \details Jams the processor, being \e RESET the only way to resume the
 * processor's execution.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \no_cycles
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_jam(bets6502* mp);

/**
 * \brief Load both A and X with the contents of memory.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e LDA and \e LDX.
 *
 * \cycles_once
 *
 * \sa bets6502_lda
 * \sa bets6502_ldx
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_lax(bets6502* mp);

/**
 * \brief Rotate left the contents of memory and then ANDs the result with A.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e ROL and \e AND.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_rol
 * \sa bets6502_and
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_rla(bets6502* mp);

/**
 * \brief Rotate right the contents of memory; Add the result to A (with carry).
 *
 * \note This instruction inherits the decimal flag dependency from ADC.
 *
 * \remark This instruction has as sub-instructions \e ROR and \e ADC.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_ror
 * \sa bets6502_adc
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_rra(bets6502* mp);

/**
 * \brief Store the result of (A & X) into memory.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e STA and \e STX.
 *
 * \cycles_once
 *
 * \sa bets6502_sta
 * \sa bets6502_stx
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_sax(bets6502* mp);

/**
 * \brief Perform logical AND on A and X, but leaving A's contents intact;
 * Subtract the result with the contents of memory and store the result on X.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e CMP and \e DEX.
 * \remark The subtraction (with carry) of the contents of memory ends up
 * working like CMP, except for storing the result in a register.
 *
 * \cycles_once
 *
 * \sa bets6502_cmp
 * \sa bets6502_dex
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_sbx(bets6502* mp);

/**
 * \brief Shift left the contents of memory and then ORs the result with A.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e ASL and \e ORA.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_asl
 * \sa bets6502_ora
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_slo(bets6502* mp);

/**
 * \brief Shift right the contents of memory and then XORs the result with A.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \remark This instruction has as sub-instructions \e LSR and \e EOR.
 *
 * \cycles_fn{three}
 *
 * \sa bets6502_lsr
 * \sa bets6502_eor
 *
 * \ingroup cpu_inst_ill
 */
void bets6502_sre(bets6502* mp);

/** \} */  // End of Stable opcodes

#endif  // !BETSNES_MICROPROCESSOR_6502_H