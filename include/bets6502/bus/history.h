#ifndef BETS_BUS_ACTIVITIES_H
#define BETS_BUS_ACTIVITIES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

struct betsBusActivity {
  uint16_t addr;
  uint8_t val;
  bool rw;
};

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4200)  // zero-sized array
#endif
struct betsBusHistory {
  size_t sz;
  size_t rptr;
  size_t wptr;
  struct betsBusActivity data[];
};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

struct betsBusHistory* betsBusHistory_new(size_t size);

void betsBusHistory_add(struct betsBusHistory* hist, bool rw,
                               uint16_t addr, uint8_t val);
struct betsBusActivity* betsBusHistory_get(
    struct betsBusHistory* hist);

void betsBusHistory_clear(struct betsBusHistory* hist);
void betsBusHistory_reset(struct betsBusHistory* hist);

#ifdef __cplusplus
}
#endif

#endif