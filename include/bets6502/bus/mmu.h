#ifndef BETS_BUS_MMU_H
#define BETS_BUS_MMU_H

#include <stdbool.h>
#include <stdint.h>

typedef uint8_t (*iofn_rd)(void* dev, uint16_t addr);
typedef void (*iofn_wr)(void* dev, uint16_t addr, uint8_t data);

struct betsMemoryInterval {
  uint16_t begin;
  uint16_t end;
};

struct betsRegion {
  const struct betsMemoryInterval mint;  // (M)emory (Int)erval
  const char id[4];
  void* obj;
  iofn_rd io_rd;
  iofn_wr io_wr;
};

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4200)  // zero-sized array
#endif
struct betsMMU {
  const size_t capacity;
  size_t size;
  struct betsRegion regions[];
};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

struct betsMMU* betsMMU_new(size_t capacity);

bool betsMMU_regions_is_sorted(const struct betsMMU* mmu);
void betsMMU_sort_regions(struct betsRegion* arr, size_t low, size_t high);

void betsMMU_build(struct betsMMU* mmu, const struct betsRegion* arr,
                   size_t arr_start, size_t arr_end);
void betsMMU_build_sorted(struct betsMMU* mmu, const struct betsRegion* regions,
                          size_t rstart, size_t rend);
void betsMMU_build_s(struct betsMMU* mmu, const struct betsRegion* regions,
                     size_t rstart, size_t rend);

uint16_t betsMMU_register_region(struct betsMMU* mmu, uint16_t begin,
                                 uint16_t end, const char* id, void* const obj,
                                 iofn_rd io_rd, iofn_wr io_wr);
uint16_t betsMMU_register_region_sorted(struct betsMMU* mmu, uint16_t begin,
                                        uint16_t end, const char* id,
                                        void* const obj, iofn_rd io_rd,
                                        iofn_wr io_wr);
uint16_t betsMMU_register_region_s(struct betsMMU* mmu, uint16_t begin,
                                   uint16_t end, const char* id,
                                   void* const obj, iofn_rd io_rd,
                                   iofn_wr io_wr);

struct betsRegion* betsMMU_get_region_at(struct betsMMU* mmu, uint16_t addr);
void* betsMMU_get_obj_at(struct betsMMU* mmu, uint16_t addr);

#endif  // !BETS_BUS_MMU_H
