#ifndef BETS_BUS_MEMORY_H
#define BETS_BUS_MEMORY_H

#include <stdint.h>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4200)  // zero-sized array
#endif
/**
 * \struct betsMemory memory.h <bets6502/bus/memory.h>
 *
 * \brief Memory structure to be attached to the \ref Bus "bus".
 * \details Virtual memory which is used by the CPU and other devices. The
 * memory is managed by the \ref betsMMU "MMU", assigned to a region where the
 * reads and writes will be performed.
 *
 * \note Due to having a variable-length array, it may spill out warnings
 * depending on your compiler's warning level.
 * 
 * \todo Suppress VLA warning in most compilers.
 */
struct betsMemory {
  const size_t sz;  ///< Size of the memory buffer.
  uint8_t data[];   ///< Data buffer.
};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

/**
 * \brief Allocate a memory object of given size.
 *
 * \param[in] size Size of memory (in bytes).
 * 
 * \return The allocated betsMemory reference.
 */
struct betsMemory* betsMemory_new(size_t size);

/**
 * \brief Clear the data stored on memory.
 *
 * \param[in] mem Memory to be cleared.
 */
void betsMemory_clear(struct betsMemory* mem);

#endif  // BETS_BUS_MEMORY_H