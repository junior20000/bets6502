#ifndef BETS_BUS_H
#define BETS_BUS_H

#include <bets6502/bus/history.h>
#include <bets6502/bus/memory.h>
#include <bets6502/bus/mmu.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct Bus {
  struct betsMemory* mem;
  struct betsBusHistory* hist;
  struct betsMMU* mmu;
} Bus_t;

void betsBus_init(Bus_t* bus, size_t mem_sz, size_t num_regions,
                  size_t hist_len);
void betsBus_init1(Bus_t* bus, size_t num_regions, size_t hist_len,
                   struct betsMemory* mem);
void betsBus_free(Bus_t* bus);
void betsBus_free1(Bus_t* bus);

void betsBus_write(const Bus_t* bus, uint16_t addr, uint8_t data);
uint8_t betsBus_read(const Bus_t* bus, uint16_t addr);

#endif  // !BETS_BUS_H
