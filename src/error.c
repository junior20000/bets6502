#include <bets6502/error.h>
#include <stdlib.h>

static bets6502Error err_inst = {0, "", NULL};

bets6502ErrorCallback bets6502Error_get_callback(void) {
  return err_inst.cb;
}

void bets6502Error_set_callback(bets6502ErrorCallback cb) { err_inst.cb = cb; }

bets6502ErrorCode bets6502_get_error(void) { return err_inst.err; }

const char* bets6502_get_error_desc(void) { return err_inst.desc; }

void bets6502_set_error(bets6502ErrorCode code, const char* desc) {
  err_inst.err = code;
  err_inst.desc = desc;

  if (err_inst.cb) {
    (*err_inst.cb)(code, desc);
  }
}

void bets6502_clear_error(void) {
  err_inst.err = BETS6502_NO_ERROR;
  err_inst.desc = "";
  err_inst.cb = NULL;
}