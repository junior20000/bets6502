/************************************************************************/
/* Doxygen documentation                                                */
/************************************************************************/
/**
 * \defgroup cpu_intern CPU Internals
 * \brief Functions used internally.
 *
 * This page is the reference documentation for the functions used internally
 * (\c static and \c inline) for convenience and better organization.
 */

#include "bets6502/6502.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

/**
 * \name Interrupt vectors
 *
 * \{
 */

/**
 * \brief Non-Maskable Interrupt vector ($FFFA-FFFB).
 *
 * \ingroup cpu_int
 */
#define BETS6502_VECTOR_NMI 0xFFFA
/**
 * \brief Reset vector ($FFFC-FFFD).
 *
 * \ingroup cpu_int
 */
#define BETS6502_VECTOR_RESET 0xFFFC
/**
 * \brief Interrupt Request vector ($FFFE-FFFF).
 *
 * \ingroup cpu_int
 */
#define BETS6502_VECTOR_IRQ 0xFFFE

/** \} */  // End of Interrupt vectors

/**
 * \brief Stack pointer start.
 *
 * \ingroup cpu_intern
 */
#define BETS6502_STACK_RESET 0xFD
/**
 * \brief Stack base memory address.
 *
 * \ingroup cpu_intern
 */
#define BETS6502_STACK_BEGIN 0X0100

/**
 * \brief Bit value.
 *
 * \ingroup cpu_intern
 */
typedef _Bool bit;

const bets6502Instruction bets6502_instset[256] = {
    {&bets6502_am_imp, &bets6502_brk},
    {&bets6502_am_idx, &bets6502_ora},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_ora},
    {&bets6502_am_zpg, &bets6502_asl},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_php},
    {&bets6502_am_imm, &bets6502_ora},
    {&bets6502_am_a, &bets6502_asl_a},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_ora},
    {&bets6502_am_abs, &bets6502_asl},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bpl},
    {&bets6502_am_idy, &bets6502_ora},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_ora},
    {&bets6502_am_zpx, &bets6502_asl},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_clc},
    {&bets6502_am_aby, &bets6502_ora},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_ora},
    {&bets6502_am_abx_rmw, &bets6502_asl},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_abs, &bets6502_jsr},
    {&bets6502_am_idx, &bets6502_and},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_bit},
    {&bets6502_am_zpg, &bets6502_and},
    {&bets6502_am_zpg, &bets6502_rol},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_plp},
    {&bets6502_am_imm, &bets6502_and},
    {&bets6502_am_a, &bets6502_rol_a},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_bit},
    {&bets6502_am_abs, &bets6502_and},
    {&bets6502_am_abs, &bets6502_rol},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bmi},
    {&bets6502_am_idy, &bets6502_and},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_and},
    {&bets6502_am_zpx, &bets6502_rol},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_sec},
    {&bets6502_am_aby, &bets6502_and},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_and},
    {&bets6502_am_abx_rmw, &bets6502_rol},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imp, &bets6502_rti},
    {&bets6502_am_idx, &bets6502_eor},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_eor},
    {&bets6502_am_zpg, &bets6502_lsr},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_pha},
    {&bets6502_am_imm, &bets6502_eor},
    {&bets6502_am_a, &bets6502_lsr_a},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_jmp},
    {&bets6502_am_abs, &bets6502_eor},
    {&bets6502_am_abs, &bets6502_lsr},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bvc},
    {&bets6502_am_idy, &bets6502_eor},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_eor},
    {&bets6502_am_zpx, &bets6502_lsr},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_cli},
    {&bets6502_am_aby, &bets6502_eor},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_eor},
    {&bets6502_am_abx_rmw, &bets6502_lsr},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imp, &bets6502_rts},
    {&bets6502_am_idx, &bets6502_adc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_adc},
    {&bets6502_am_zpg, &bets6502_ror},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_pla},
    {&bets6502_am_imm, &bets6502_adc},
    {&bets6502_am_a, &bets6502_ror_a},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_ind, &bets6502_jmp},
    {&bets6502_am_abs, &bets6502_adc},
    {&bets6502_am_abs, &bets6502_ror},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bvs},
    {&bets6502_am_idy, &bets6502_adc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_adc},
    {&bets6502_am_zpx, &bets6502_ror},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_sei},
    {&bets6502_am_aby, &bets6502_adc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_adc},
    {&bets6502_am_abx_rmw, &bets6502_ror},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_idx, &bets6502_sta},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_sty},
    {&bets6502_am_zpg, &bets6502_sta},
    {&bets6502_am_zpg, &bets6502_stx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_dey},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_txa},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_sty},
    {&bets6502_am_abs, &bets6502_sta},
    {&bets6502_am_abs, &bets6502_stx},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bcc},
    {&bets6502_am_idy_rmw, &bets6502_sta},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_sty},
    {&bets6502_am_zpx, &bets6502_sta},
    {&bets6502_am_zpy, &bets6502_stx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_tya},
    {&bets6502_am_aby_rmw, &bets6502_sta},
    {&bets6502_am_imp, &bets6502_txs},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx_rmw, &bets6502_sta},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imm, &bets6502_ldy},
    {&bets6502_am_idx, &bets6502_lda},
    {&bets6502_am_imm, &bets6502_ldx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_ldy},
    {&bets6502_am_zpg, &bets6502_lda},
    {&bets6502_am_zpg, &bets6502_ldx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_tay},
    {&bets6502_am_imm, &bets6502_lda},
    {&bets6502_am_imp, &bets6502_tax},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_ldy},
    {&bets6502_am_abs, &bets6502_lda},
    {&bets6502_am_abs, &bets6502_ldx},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bcs},
    {&bets6502_am_idy, &bets6502_lda},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_ldy},
    {&bets6502_am_zpx, &bets6502_lda},
    {&bets6502_am_zpy, &bets6502_ldx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_clv},
    {&bets6502_am_aby, &bets6502_lda},
    {&bets6502_am_imp, &bets6502_tsx},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_ldy},
    {&bets6502_am_abx, &bets6502_lda},
    {&bets6502_am_aby, &bets6502_ldx},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imm, &bets6502_cpy},
    {&bets6502_am_idx, &bets6502_cmp},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_cpy},
    {&bets6502_am_zpg, &bets6502_cmp},
    {&bets6502_am_zpg, &bets6502_dec},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_iny},
    {&bets6502_am_imm, &bets6502_cmp},
    {&bets6502_am_imp, &bets6502_dex},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_cpy},
    {&bets6502_am_abs, &bets6502_cmp},
    {&bets6502_am_abs, &bets6502_dec},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_bne},
    {&bets6502_am_idy, &bets6502_cmp},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_cmp},
    {&bets6502_am_zpx, &bets6502_dec},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_cld},
    {&bets6502_am_aby, &bets6502_cmp},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_cmp},
    {&bets6502_am_abx_rmw, &bets6502_dec},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_imm, &bets6502_cpx},
    {&bets6502_am_idx, &bets6502_sbc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpg, &bets6502_cpx},
    {&bets6502_am_zpg, &bets6502_sbc},
    {&bets6502_am_zpg, &bets6502_inc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_inx},
    {&bets6502_am_imm, &bets6502_sbc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abs, &bets6502_cpx},
    {&bets6502_am_abs, &bets6502_sbc},
    {&bets6502_am_abs, &bets6502_inc},
    {&bets6502_am_imp, &bets6502_nop},
    /////////////////////////////////////////////
    {&bets6502_am_rel, &bets6502_beq},
    {&bets6502_am_idy, &bets6502_sbc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_zpx, &bets6502_sbc},
    {&bets6502_am_zpx, &bets6502_inc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_sed},
    {&bets6502_am_aby, &bets6502_sbc},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_imp, &bets6502_nop},
    {&bets6502_am_abx, &bets6502_sbc},
    {&bets6502_am_abx_rmw, &bets6502_inc},
    {&bets6502_am_imp, &bets6502_nop},
};

/**
 * \brief The CPU instruction table.
 *
 * This table contains all the instructions (legal and illegal) of the 6502
 * microprocessor, where the indexes of the table are the operation code of the
 * instruction.
 *
 * \ingroup cpu_intern
 */

/**
 * \name I/O
 * \brief I/O related inline functions.
 *
 * \{
 */

/**
 * \brief \copybrief bets6502_read
 *
 * Start a \e READ memory cycle from the bus.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The memory address to read from.
 *
 * \cycles_once
 *
 * \sa \ref bets6502_read
 *
 * \ingroup cpu_intern
 */
inline uint8_t bets6502_read_inl(bets6502* mp, uint16_t addr) {
  bets6502_tick(mp);
  return betsBus_read(mp->bus, addr);
}

/**
 * \brief \copybrief bets6502_write
 *
 * Start a \e WRITE memory cycle from the bus.
 *
 * \param[in,out] mp   The 6502 instance.
 * \param[in]     addr The memory address to write to.
 * \param[in]     data The data to be written.
 *
 * \cycles_once
 *
 * \sa \ref bets6502_write
 *
 * \ingroup cpu_intern
 */
inline void bets6502_write_inl(bets6502* mp, uint16_t addr, uint8_t data) {
  bets6502_tick(mp);
  betsBus_write(mp->bus, addr, data);
}

/** \} */  // End of I/O

/**
 * \name Stack memory operations
 * \brief Stack memory operations inline functions.
 *
 * \{
 */

/**
 * \brief \copybrief bets6502_spush
 *
 * \sa bets6502_spush
 *
 * \ingroup cpu_intern
 */
inline void bets6502_spush_inl(bets6502* mp, uint8_t data) {
  bets6502_write_inl(mp, BETS6502_STACK_BEGIN + mp->stkptr, data);
  mp->stkptr--;
}

/**
 * \brief Pop U8 value from Stack.
 *
 * \sa bets6502_spush
 *
 * \ingroup cpu_intern
 */
inline uint8_t bets6502_spop_inl(bets6502* mp) {
  mp->stkptr++;
  return bets6502_read_inl(mp, BETS6502_STACK_BEGIN + mp->stkptr);
}

/** \} */  // End of Stack memory operations

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4804)  // Unsafe usage of bool
#endif
/**
 * \brief Set the state of the chosen bit.
 *
 * \param[in,out] mp The 6502 instance with the P register to be updated.
 * \param[in]     flag The bit to be updated.
 * \param[in]     state Value of the bit.
 *
 * \ingroup cpu_intern
 */
inline void set_status(bets6502* mp, ProcessorStatus_t flag, bit state) {
  mp->status ^= (-state ^ mp->status) & flag;
}
#ifdef _MSC_VER
#pragma warning(pop)
#endif

/**
 * \brief Get the state of the chosen bit.
 *
 * \param[in,out] mp The 6502 instance with the P register to be retrieved.
 * \param[in]     flag The bit to be retrieved.
 *
 * \return \c true if the bit is set; \c false otherwise.
 *
 * \ingroup cpu_intern
 */
inline bool get_status(const bets6502* mp, ProcessorStatus_t flag) {
  return ((mp->status & flag) ? 1 : 0);
}

/**
 * \brief Set the chosen flag.
 *
 * \param[in,out] mp The 6502 instance with the P register to be set.
 * \param[in]     flag The bit to be set.
 *
 * \ingroup cpu_intern
 */
inline void set_flag(bets6502* mp, ProcessorStatus_t flag) {
  mp->status |= flag;
}

/**
 * \brief Clear the chosen flag.
 *
 * \param[in,out] mp The 6502 instance with the P register to be cleared.
 * \param[in]     flag The bit to be cleared.
 *
 * \ingroup cpu_intern
 */
inline void clear_flag(bets6502* mp, ProcessorStatus_t flag) {
  mp->status &= ~flag;
}

/**
 * \brief Check if a page boundary cross will happen.
 * \details A page cross happens when the both higher bits of current and future
 * memory access differs.
 *
 * \param[in] addr Base address.
 * \param[in] idx Index register value offset.
 *
 * \return \c true if a page was crossed; \c false otherwise.
 *
 * \ingroup cpu_intern
 */
inline bool can_cross(uint16_t addr, uint8_t idx) {
  return ((addr + idx) & 0xFF00) != (addr & 0xFF00);
}

/**
 * \brief Check if a page boundary cross will happen.
 * \details A page cross happens when the both higher bits of current and future
 * memory access differs. Differently from \ref can_cross, this function permits
 * backward page-crossing.
 *
 * \note This function is only used in branch functions.
 *
 * \param[in] addr Base address.
 * \param[in] idx Index register value offset.
 *
 * \return \c true if a page was crossed; \c false otherwise.
 *
 * \ingroup cpu_intern
 */
inline bool can_cross_s8(uint16_t addr, int8_t idx) {
  return ((addr + idx) & 0xFF00) != (addr & 0xFF00);
}

/**
 * \brief Update \ref ProcessorStatus.kZ "Z" and \ref ProcessorStatus.kN "N"
 * flags.
 *
 * \note This is a convenience function used by all functions which both flags
 * are affected.
 *
 * \param[in,out] mp   The 6502 instance with the P register to be updated.
 * \param[in]     data The data to determine the value of the registers.
 *
 * \ingroup cpu_intern
 */
inline void update_zn(bets6502* mp, uint8_t data) {
  set_status(mp, kZ, data == 0);
  set_status(mp, kN, data & 0x80);
}

/**
 * \brief Clear \ref ProcessorStatus.kB "B" and \ref ProcessorStatus.kU "U"
 * flags.
 *
 * \param[in,out] mp The 6502 instance.
 *
 * \ingroup cpu_intern
 */
inline void clear_bu(bets6502* mp) {
  clear_flag(mp, kB);
  clear_flag(mp, kU);
}

void bets6502_init(bets6502* mp) {
  if (!mp) {
    bets6502_set_error(BETS6502_NULL_POINTER,
                       "Could not initialize the bets6502 object. Null pointer "
                       "passed as argument.");

    return;
  }

  mp->mar = 0;
  mp->mdr = 0;
  mp->ir = 0;

  mp->acc = 0;
  mp->x = 0;
  mp->y = 0;
  mp->status = kU;  // Status flag is always set
  mp->stkptr = 0;
  mp->pc = 0;
  mp->cycles = 0;

  mp->is_irq_pending = false;
  mp->is_nmi_pending = false;
  mp->is_res_pending = false;
  mp->is_jammed = false;

  // TODO: Add a tick logic dispatch table for each instruction.
  mp->tick_logic = NULL;
}

// TODO: ALLOW CUSTOM DEALLOCATOR
void bets6502_free(bets6502* mp) {
  betsBus_free(mp->bus);

  mp->bus = NULL;
}

uint8_t bets6502_read(bets6502* mp, uint16_t addr) {
  return bets6502_read_inl(mp, addr);
}

uint16_t bets6502_readU16(bets6502* mp, uint16_t adl, uint16_t adh) {
  return (bets6502_read_inl(mp, adl) |
          (uint16_t)(bets6502_read_inl(mp, adh) << 8));
}

uint16_t bets6502_readU16_s(bets6502* mp, uint16_t addr) {
  return bets6502_readU16(mp, addr, addr + 1);
}

void bets6502_write(bets6502* mp, uint16_t addr, uint8_t data) {
  bets6502_write_inl(mp, addr, data);
}

void bets6502_writeU16(bets6502* mp, uint16_t addr, uint16_t data) {
  bets6502_write_inl(mp, addr, data & 0xFF);    // Write low-byte
  bets6502_write_inl(mp, addr + 1, data >> 8);  // Write high-byte
}

void bets6502_spush(bets6502* mp, uint8_t data) {
  bets6502_spush_inl(mp, data);
}

void bets6502_spushU16(bets6502* mp, uint16_t data) {
  bets6502_spush_inl(mp, data >> 8);
  bets6502_spush_inl(mp, data & 0xFF);
}

uint8_t bets6502_spop(bets6502* mp) { return bets6502_spop_inl(mp); }

uint16_t bets6502_spopU16(bets6502* mp) {
  register uint8_t data = bets6502_spop_inl(mp);
  return (uint16_t)((uint16_t)bets6502_spop_inl(mp) << 8) | data;
}

/**
 * \brief Fetch data from memory, pointed by MAR, to the provided register.
 *
 * \param[in,out] mp The 6502 instance with the MAR register to read from.
 * \param[out] reg The register to fetch data to.
 *
 * \cycles_once
 *
 * \ingroup cpu_intern
 */
inline void bets6502_fetch(bets6502* mp, uint8_t* reg) {
  *reg = bets6502_read_inl(mp, mp->mar);
}

void bets6502_tick(bets6502* mp) {
  if (mp->tick_logic) {
    mp->tick_logic(mp);
  }

  mp->cycles++;
}

void bets6502_reset(bets6502* mp) {
  bets6502_tick(mp);
  bets6502_tick(mp);

  mp->status = (0 | kU) | kI;
  mp->stkptr = BETS6502_STACK_RESET;

  bets6502_tick(mp);
  bets6502_tick(mp);
  bets6502_tick(mp);

  mp->mar = BETS6502_VECTOR_RESET;
  mp->pc = bets6502_readU16_s(mp, mp->mar);
}

/**
 * \brief Trigger a hardware interrupt.
 *
 * \details This is a template for IRQ and NMI.
 *
 * \param[in,out] mp  The 6502 instance.
 * \param[in]     vec The interrupt vector begin address.
 *
 * \cycles This macro expansion adds \b six cycles.
 *
 * \sa bets6502_irq
 * \sa bets6502_nmi
 *
 * \ingroup cpu_intern
 */
#define BETS6502_HWINT(mp, vec)                                          \
  bets6502_tick((mp));               /* T1     - Read PC and discard. */ \
  bets6502_spushU16((mp), (mp->pc)); /* T2, T3 - Push PC to Stack.    */ \
                                                                         \
  set_flag((mp), kU);                                                    \
                                                                         \
  bets6502_spush_inl((mp), (mp)->status); /* T4 - Push P to Stack. */    \
                                                                         \
  set_flag((mp), kI);                                                    \
                                                                         \
  (mp)->mar = (vec);                                                     \
  /* T5 - Read the interrupt vector and fetch the data to PC. */         \
  (mp)->pc = bets6502_readU16_s((mp), (mp)->mar)

void bets6502_irq(bets6502* mp) {
  if (!get_status(mp, kI)) {
    BETS6502_HWINT(mp, BETS6502_VECTOR_IRQ);
  }
}

void bets6502_nmi(bets6502* mp) { BETS6502_HWINT(mp, BETS6502_VECTOR_NMI); }

#undef BETS6502_HWINT

bool bets6502_get_status(const bets6502* mp, ProcessorStatus_t status) {
  assert(mp);

  return get_status(mp, status);
}

void bets6502_set_status(bets6502* mp, ProcessorStatus_t status, bit state) {
  assert(mp);

  set_status(mp, status, state);
}

/************************************************************************/
/* Addressing modes                                                     */
/************************************************************************/

void bets6502_am_a(bets6502* mp) { (void)mp; }

void bets6502_am_imm(bets6502* mp) { mp->mar = mp->pc++; }

void bets6502_am_imp(bets6502* mp) { (void)mp; }

void bets6502_am_rel(bets6502* mp) { mp->mar = mp->pc++; }

void bets6502_am_abs(bets6502* mp) {
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;
}

void bets6502_am_zpg(bets6502* mp) {
  mp->mar = bets6502_read_inl(mp, mp->pc);  // T1 - Fetch effective address.
  mp->pc++;
}

void bets6502_am_ind(bets6502* mp) {
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;

  // Hardware JMP with page cross bug
  if ((mp->mar & 0xFF) == 0xFF) {
    mp->mar = bets6502_readU16(mp, mp->mar, mp->mar & 0xFF00);
  } else {
    mp->mar = bets6502_readU16_s(mp, mp->mar);
  }
}

void bets6502_am_abx(bets6502* mp) {
  // T1, T2 - Fetch effective address.
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;

  if (can_cross(mp->mar, mp->x)) {
    bets6502_tick(mp);  // T3 - Extra tick due to fetching data from next page.
  }

  mp->mar += mp->x;
}

void bets6502_am_abx_rmw(bets6502* mp) {
  // T1, T2 - Fetch effective address.
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;

  bets6502_tick(mp);  // T3 - Data discard.

  mp->mar += mp->x;
}

void bets6502_am_aby(bets6502* mp) {
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;

  if (can_cross(mp->mar, mp->y)) {
    bets6502_tick(mp);
  }

  mp->mar += mp->y;
}

void bets6502_am_zpx(bets6502* mp) {
  // T1 - Fetch base address (low)
  mp->mar = bets6502_read_inl(mp, mp->pc);
  mp->pc++;
  bets6502_tick(mp);  // T2 - Simulate data discard.

  // Adds the X offset; Ticks in the begining of next instruction.
  mp->mar = (mp->mar + mp->x) & 0xFF;
}

void bets6502_am_zpy(bets6502* mp) {
  // T1 - Fetch base address (low)
  mp->mar = bets6502_read_inl(mp, mp->pc);
  mp->pc++;
  bets6502_tick(mp);  // T2 - Simulate data discard.

  // Adds the Y offset; Ticks in the begining of next instruction.
  mp->mar = (mp->mar + mp->y) & 0xFF;
}

void bets6502_am_idx(bets6502* mp) {
  mp->mar = bets6502_read_inl(mp, mp->pc);  // T1 - Fetch zero page indirect
                                            // address.
  mp->pc++;

  bets6502_tick(mp);  // T2 - Simulate data discard.

  mp->mar = (mp->mar + mp->x) & 0xFF;  // Add the X offset to BAL; This
                                       // operation is part of the next tick.

  // T3, T4 - Fetch the effective address from (BAL + X) and (BAL + X + 1).
  mp->mar = bets6502_readU16(mp, mp->mar, (mp->mar + 1) & 0xFF);
}

void bets6502_am_idy(bets6502* mp) {
  mp->mar = bets6502_read_inl(mp, mp->pc);  // T1 - Fetch zero page
                                            // indirect address.
  mp->pc++;

  // T2, T3 - Fetch base address.
  mp->mar = bets6502_readU16(mp, mp->mar, (mp->mar + 1) & 0xFF);

  if (can_cross(mp->mar, mp->y)) {
    bets6502_tick(mp);  // T4 - Extra tick due to fetching data from next page.
  }

  mp->mar += mp->y;
}

/************************************************************************/
/* Unintended addressing modes                                          */
/************************************************************************/

void bets6502_am_aby_rmw(bets6502* mp) {
  // T1, T2 - Fetch effective address
  mp->mar = bets6502_readU16_s(mp, mp->pc);
  mp->pc += 2;

  mp->mar += mp->y;
  bets6502_tick(mp);  // T3 - Read (dummy)
}

void bets6502_am_idy_rmw(bets6502* mp) {
  mp->mar = bets6502_read_inl(mp, mp->pc);  // T1 - Fetch zero page
                                            // indirect address.
  mp->pc++;

  // T2, T3 - Fetch base address
  mp->mar = bets6502_readU16(mp, mp->mar, (mp->mar + 1) & 0xFF);

  bets6502_tick(mp);  // T4 - Read (dummy)
  mp->mar += mp->y;
}

/************************************************************************/
/* Instructions                                                         */
/************************************************************************/

#define BETS6502_AND(mp)              \
  do {                                \
    bets6502_fetch((mp), &(mp)->mdr); \
    (mp)->acc &= (mp)->mdr;           \
  } while (0)

/**
 * \brief Fetch data from memory into the specified register. Zero and Negative
 * flags are updated upon the value loaded.
 *
 * \note This function acts as a template for all the LDx instructions.
 *
 * \param[in,out] mp  The 6502 instance with the registers where the operation
 * will be performed. \param[in,out] reg The register to load thereof.
 *
 * \cycles_once
 *
 * \sa bets6502_lda
 * \sa bets6502_ldx
 * \sa bets6502_ldy
 *
 * \ingroup cpu_intern
 */
inline void bets6502_ld(bets6502* mp, uint8_t* reg) {
  bets6502_fetch(mp, reg);

  update_zn(mp, *reg);
}

void bets6502_lda(bets6502* mp) { bets6502_ld(mp, &mp->acc); }

void bets6502_ldx(bets6502* mp) { bets6502_ld(mp, &mp->x); }

void bets6502_ldy(bets6502* mp) { bets6502_ld(mp, &mp->y); }

void bets6502_sta(bets6502* mp) { bets6502_write_inl(mp, mp->mar, mp->acc); }

void bets6502_stx(bets6502* mp) { bets6502_write_inl(mp, mp->mar, mp->x); }

void bets6502_sty(bets6502* mp) { bets6502_write_inl(mp, mp->mar, mp->y); }

/**
 * \brief Copies values between two registers and update the N and Z flags as
 * appropriate.
 *
 * \note This function act as a template for Txx instructions.
 * \attention Differently from what the name suggests, the values are not
 * transferred but copied.
 *
 * \param[in,out] mp   The 6502 instance with the registers where the operation
 * will be performed.
 *
 * \param[in]     src  The register to be copied.
 * \param[in,out] dest The register to receive the value copied.
 *
 * \cycles_once
 *
 * \sa bets6502_tax
 * \sa bets6502_tay
 * \sa bets6502_txa
 * \sa bets6502_tya
 *
 * \ingroup cpu_intern
 */
inline void bets6502_transfer(bets6502* mp, const uint8_t src, uint8_t* dest) {
  *dest = src;
  update_zn(mp, *dest);
  bets6502_tick(mp);
}

void bets6502_tax(bets6502* mp) { bets6502_transfer(mp, mp->acc, &mp->x); }

void bets6502_tay(bets6502* mp) { bets6502_transfer(mp, mp->acc, &mp->y); }

void bets6502_txa(bets6502* mp) { bets6502_transfer(mp, mp->x, &mp->acc); }

void bets6502_tya(bets6502* mp) { bets6502_transfer(mp, mp->y, &mp->acc); }

void bets6502_tsx(bets6502* mp) { bets6502_transfer(mp, mp->stkptr, &mp->x); }

void bets6502_txs(bets6502* mp) {
  mp->stkptr = mp->x;
  bets6502_tick(mp);  // T1 - Copy X to S.
}

void bets6502_pha(bets6502* mp) {
  bets6502_tick(mp);
  bets6502_spush_inl(mp, mp->acc);
}

void bets6502_php(bets6502* mp) {
  bets6502_tick(mp);                             // T1 - PC+1 dummy read.
  bets6502_spush_inl(mp, mp->status | kU | kB);  // T2 - Push P into Stack.
}

void bets6502_pla(bets6502* mp) {
  bets6502_tick(mp);
  bets6502_tick(mp);
  mp->acc = bets6502_spop_inl(mp);
  update_zn(mp, mp->acc);
}

void bets6502_plp(bets6502* mp) {
  bets6502_tick(mp);  // T1 - PC+1 dummy read.
  bets6502_tick(mp);  // T2 - Stack pointer dummy read.

  mp->status = bets6502_spop_inl(mp);  // T3 - Pull P from Stack and
  clear_bu(mp);                        // clear B and U flags.
}

void bets6502_and(bets6502* mp) {
  BETS6502_AND(mp);
  update_zn(mp, mp->acc);
}

void bets6502_eor(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);
  mp->acc ^= mp->mdr;
  update_zn(mp, mp->acc);
}

void bets6502_ora(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);
  mp->acc |= mp->mdr;
  update_zn(mp, mp->acc);
}

void bets6502_bit(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data.

  set_status(mp, kZ, !(mp->mdr & mp->acc));
  set_status(mp, kN, mp->mdr & 0x80);
  set_status(mp, kV, mp->mdr & 0x40);
}

/**
 * \brief ADC bin flags update as boilerplate.
 *
 * Update the flags affected ADC in non-decimal (binary) mode.
 *
 * \param[in, out] mp     The 6502 instance.
 * \param[in]      result The result of (A + mem + C).
 *
 * \sa bets6502_adc
 *
 * \ingroup cpu_intern
 */
#define BETS6502_ADC_FLAGS_BINARY(mp, result) \
  set_status((mp), kC, (result) > 0xFF);      \
  update_zn((mp), (uint8_t)(result));         \
  set_status((mp), kV, ~((mp)->acc ^ (mp)->mdr) & ((mp)->acc ^ (result)) & 0x80)

/**
 * \brief SBC bin flags update as boilerplate.
 *
 * Update the flags affected SBC in non-decimal (binary) mode.
 *
 * \param[in, out] mp     The 6502 instance.
 * \param[in]      result The result of (A - mem - !C).
 *
 * \sa bets6502_sbc
 *
 * \ingroup cpu_intern
 */
#define BETS6502_SBC_FLAGS_BINARY(mp, result)     \
  set_status(mp, kC, (uint16_t)(result) < 0x100); \
  update_zn(mp, (uint8_t)(result));               \
  set_status(                                     \
      mp, kV,                                     \
      ((((mp)->acc ^ (result)) & 0x80) && (((mp)->acc ^ (mp)->mdr) & 0x80)))

void bets6502_adc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);
  uint16_t register result = mp->acc + mp->mdr + (uint8_t)get_status(mp, kC);

#ifdef BETS6502_WITH_DECIMAL_MODE

  if (get_status(mp, kD)) {
    // The Z flag is updated upon the binary result
    set_status(mp, kZ, (uint8_t)result == 0);

    result = (mp->acc & 0x0F) + (mp->mdr & 0x0F) + (uint8_t)get_status(mp, kC);

    // Fix lower nibble
    if (result > 0x09) result = ((result + 6) & 0x0F) + 0x10;

    result += ((mp->acc & 0xF0) + (mp->mdr & 0xF0));

    set_status(mp, kN, (result & 0x80));
    set_status(mp, kV, ~(mp->acc ^ mp->mdr) & (mp->acc ^ result) & 0x80);

    // Fix higher nibble
    if (result > 0x9F) result += 0x60;

    set_status(mp, kC, result > 0x9F);

    goto assing_result;
  }
#endif

  BETS6502_ADC_FLAGS_BINARY(mp, result);

assing_result:
  mp->acc = (uint8_t)result;
}

void bets6502_sbc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);
  int16_t register result = mp->acc - mp->mdr - (uint8_t)(!get_status(mp, kC));

#ifdef BETS6502_WITH_DECIMAL_MODE
  if (get_status(mp, kD)) {
    const bool oc = get_status(mp, kC);

    // Flags are updated upon the result of the binary subtraction
    BETS6502_SBC_FLAGS_BINARY(mp, result);

    result = (mp->acc & 0x0F) - (mp->mdr & 0x0F) - (!oc);

    // Fix lower nibble
    if (result < 0) result = ((result - 0x06) & 0x0F) - 0x10;

    result += ((mp->acc & 0xF0) - (mp->mdr & 0xF0));

    // Fix higher nibble
    if (result < 0) result -= 0x60;

    goto assing_result;
  }
#endif

  BETS6502_SBC_FLAGS_BINARY(mp, result);

assing_result:
  mp->acc = (uint8_t)result;
}

/**
 * \brief Compare a register with a location in memory.
 *
 * \note This function is used as a macro for the CMP/CPx instructions.
 *
 * \sa bets6502_cmp
 * \sa bets6502_cpx
 * \sa bets6502_cpy
 *
 * \ingroup cpu_intern
 */
inline void bets6502_compare(bets6502* mp, uint8_t reg) {
  bets6502_fetch(mp, &mp->mdr);  // T1 -  Fetch data from effective address
  set_status(mp, kC, reg >= mp->mdr);

  mp->mdr = reg - mp->mdr;

  update_zn(mp, mp->mdr);
}

void bets6502_cmp(bets6502* mp) { bets6502_compare(mp, mp->acc); }

void bets6502_cpx(bets6502* mp) { bets6502_compare(mp, mp->x); }

void bets6502_cpy(bets6502* mp) { bets6502_compare(mp, mp->y); }

void bets6502_inc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1   - Fetch data from effective address
  bets6502_tick(mp);             // T2   - Read (dummy)
  mp->mdr++;                     // T3/0 - Modify

  update_zn(mp, mp->mdr);
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
}

inline void bets6502_inc_reg(bets6502* mp, uint8_t* reg) {
  (*reg)++;
  update_zn(mp, *reg);
  bets6502_tick(mp);
}

void bets6502_inx(bets6502* mp) { bets6502_inc_reg(mp, &mp->x); }

void bets6502_iny(bets6502* mp) { bets6502_inc_reg(mp, &mp->y); }

void bets6502_dec(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1   - Fetch data from effective address
  bets6502_tick(mp);             // T2   - Read (dummy)
  mp->mdr--;                     // T3/0 - Modify

  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
  update_zn(mp, mp->mdr);
}

inline void bets6502_dec_reg(bets6502* mp, uint8_t* reg) {
  (*reg)--;
  update_zn(mp, *reg);
  bets6502_tick(mp);
}

void bets6502_dex(bets6502* mp) { bets6502_dec_reg(mp, &mp->x); }

void bets6502_dey(bets6502* mp) { bets6502_dec_reg(mp, &mp->y); }

void bets6502_asl(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  set_status(mp, kC, mp->mdr & 0x80);

  bets6502_tick(mp);                  // T2   - Read (dummy)
  mp->mdr = (uint8_t)(mp->mdr << 1);  // T3/0 - Modify

  update_zn(mp, mp->mdr);
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
}

void bets6502_asl_a(bets6502* mp) {
  set_status(mp, kC, mp->acc & 0x80);

  mp->acc = (uint8_t)(mp->acc << 1);

  update_zn(mp, mp->acc);

  bets6502_tick(mp);  // T1 - Apply data modification
}

void bets6502_lsr(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  set_status(mp, kC, mp->mdr & 1);

  bets6502_tick(mp);                  // T2   - Read (dummy)
  mp->mdr = (uint8_t)(mp->mdr >> 1);  // T3/0 - Modify

  set_status(mp, kZ, mp->mdr == 0);
  clear_flag(mp, kN);

  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
}

void bets6502_lsr_a(bets6502* mp) {
  set_status(mp, kC, mp->acc & 1);

  mp->acc = (uint8_t)(mp->acc >> 1);

  set_status(mp, kZ, mp->acc == 0);
  clear_flag(mp, kN);

  bets6502_tick(mp);  // T1 - Apply data modification
}

void bets6502_rol(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  uint8_t old_c = get_status(mp, kC);

  set_status(mp, kC, mp->mdr & 0x80);

  bets6502_tick(mp);                          // T2   - Read (dummy)
  mp->mdr = (uint8_t)(mp->mdr << 1 | old_c);  // T3/0 - Modify

  update_zn(mp, mp->mdr);
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
}

void bets6502_rol_a(bets6502* mp) {
  mp->mdr = get_status(mp, kC);
  set_status(mp, kC, mp->acc & 0x80);

  mp->acc = (uint8_t)((mp->acc << 1) | mp->mdr);

  update_zn(mp, mp->acc);

  bets6502_tick(mp);  // T1 - Apply data modification
}

void bets6502_ror(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  uint8_t old_c = (uint8_t)get_status(mp, kC) << 7;

  set_status(mp, kC, mp->mdr & 1);

  bets6502_tick(mp);                          // T2   - Read (dummy)
  mp->mdr = (uint8_t)(mp->mdr >> 1 | old_c);  // T3/0 - Modify

  update_zn(mp, mp->mdr);
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write
}

void bets6502_ror_a(bets6502* mp) {
  mp->mdr = (uint8_t)get_status(mp, kC) << 7;
  set_status(mp, kC, mp->acc & 1);

  mp->acc = (uint8_t)(mp->acc >> 1 | (mp->mdr));

  update_zn(mp, mp->acc);

  bets6502_tick(mp);  // T1 - Apply data modification
}

void bets6502_jmp(bets6502* mp) { mp->pc = mp->mar; }

void bets6502_jsr(bets6502* mp) {
  bets6502_spushU16(mp, mp->pc - 1);  // T1, T2 - Push the last part of
                                      // the instruction.
  mp->pc = mp->mar;

  bets6502_tick(mp);  // T3 - This should be the stack data dummy read, however,
                      // because of the emulator design of separating the
                      // address mode logic from the instruction logic, this
                      // tick will come last.
}

void bets6502_rts(bets6502* mp) {
  bets6502_tick(mp);                  // T1 - Dummy read of the next PC.
  bets6502_tick(mp);                  // T2 - Dummy read of the stack.
  mp->pc = bets6502_spopU16(mp) + 1;  // T3, T4 - Pop the place where JSR
                                      // was called last; Set PC to the next
                                      // location.
  bets6502_tick(mp);                  // T5 - Dummy read of the popped PC.
}

inline void bets6502_branch(bets6502* mp, ProcessorStatus_t flag, bit state) {
  if (get_status(mp, flag) == state) {
    if (can_cross_s8(mp->pc, (int8_t)mp->mdr)) {
      bets6502_tick(mp);
    }
    mp->pc += (int8_t)mp->mdr;
    bets6502_tick(mp);
  }
}

void bets6502_bcc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kC, 0);
}

void bets6502_bcs(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kC, 1);
}

void bets6502_beq(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kZ, 1);
}

void bets6502_bmi(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kN, 1);
}

void bets6502_bne(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kZ, 0);
}

void bets6502_bpl(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kN, 0);
}

void bets6502_bvc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kV, 0);
}

void bets6502_bvs(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch offset from effective address.
  bets6502_branch(mp, kV, 1);
}

void bets6502_clc(bets6502* mp) {
  mp->status &= ~kC;
  bets6502_tick(mp);
}

void bets6502_cld(bets6502* mp) {
  mp->status &= ~kD;
  bets6502_tick(mp);
}

void bets6502_cli(bets6502* mp) {
  mp->status &= ~kI;
  bets6502_tick(mp);
}

void bets6502_clv(bets6502* mp) {
  mp->status &= ~kV;
  bets6502_tick(mp);
}

void bets6502_sec(bets6502* mp) {
  mp->status |= kC;
  bets6502_tick(mp);
}

void bets6502_sed(bets6502* mp) {
  mp->status |= kD;
  bets6502_tick(mp);
}

void bets6502_sei(bets6502* mp) {
  mp->status |= kI;
  bets6502_tick(mp);
}

void bets6502_brk(bets6502* mp) {
  bets6502_tick(mp);  // T1 - (PC + 1) dummy read.
  mp->pc++;  // Due to being an instruction, the next position (PC+1) is pushed
             // to Stack.

  bets6502_spushU16(mp, mp->pc);  // T2, T3 - Push PC to Stack.

  set_flag(mp, kU);

  bets6502_spush_inl(mp, mp->status | kB);  // T4 - Push P to Stack.

  set_flag(mp, kI);

  // Interrupt hijack
  if (mp->is_nmi_pending) {
    mp->is_nmi_pending = false;
    mp->mar = BETS6502_VECTOR_NMI;
  } else {
    mp->mar = BETS6502_VECTOR_IRQ;
  }

  mp->pc = bets6502_readU16_s(mp, mp->mar);  // T5, T6 - Read the
                                             // interrupt vector.
}

void bets6502_nop(bets6502* mp) { bets6502_tick(mp); }

void bets6502_rti(bets6502* mp) {
  bets6502_tick(mp);  // T1 - PC+1 dummy read.
  bets6502_tick(mp);  // T2 - Stack pointer dummy read.

  mp->status = bets6502_spop_inl(mp);  // T3 - Pull P from Stack and
  clear_bu(mp);                        // clear B and U flags.

  mp->pc = bets6502_spopU16(mp);  // T4, T5 - Pull PC from Stack.
}

/************************************************************************/
/* Illegal opcodes                                                      */
/************************************************************************/

void bets6502_alr(bets6502* mp) {
  BETS6502_AND(mp);
  set_status(mp, kC, mp->acc & 1);

  mp->acc = (uint8_t)(mp->acc >> 1);

  set_status(mp, kZ, mp->acc == 0);
  clear_flag(mp, kN);
}

void bets6502_anc(bets6502* mp) {
  BETS6502_AND(mp);
  update_zn(mp, mp->acc);
  set_status(mp, kC, get_status(mp, kN));
}

void bets6502_arr(bets6502* mp) {
  BETS6502_AND(mp);  // T1 - Fetch immediate data.

  uint16_t register result =
      (uint8_t)(mp->acc >> 1) | (uint8_t)((uint8_t)get_status(mp, kC) << 7);

  set_status(mp, kZ, result == 0);
  set_status(mp, kV, (bool)(result & 0x40) ^ (bool)(result & 0x20));

#ifdef BETS6502_WITH_DECIMAL_MODE
  if (get_status(mp, kD)) {  // TODO: FIX DECIMAL MODE!!!
    set_status(mp, kN, get_status(mp, kC));
    clear_flag(mp, kC);

    if (((mp->acc & 0x0F) + (mp->acc & 0x01)) > 0x05) {
      result = (result & 0xF0) | ((result + 0x06) & 0x0F);
    }

    if (((mp->acc & 0xF0) + (mp->acc & 0x10)) > 0x50) {
      result = (result & 0x0F) | ((result + 0x60) & 0xF0);
      set_flag(mp, kC);
    }
    goto assign_result;
  }
#endif
  set_status(mp, kN, result & 0x80);
  set_status(mp, kC, result & 0x40);

assign_result:
  mp->acc = (uint8_t)result;
}

void bets6502_dcp(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1   - Fetch data from effective address
  bets6502_tick(mp);             // T2   - Read (dummy)
  mp->mdr--;                     // T3/0 - Modify

  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  set_status(mp, kC, mp->acc >= mp->mdr);
  update_zn(mp, mp->acc - mp->mdr);
}

void bets6502_isc(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  bets6502_tick(mp);             // T2 - Read (dummy)

  mp->mdr++;                                 // T3/0 - Modify
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  int16_t register result = mp->acc - mp->mdr - (!get_status(mp, kC));

#ifdef BETS6502_WITH_DECIMAL_MODE
  if (get_status(mp, kD)) {
    const bool oc = get_status(mp, kC);

    // Flags are updated upon the result of the binary subtraction
    set_status(mp, kC, (uint16_t)result < 0x100);
    update_zn(mp, (uint8_t)result);
    set_status(mp, kV,
               (((mp->acc ^ result) & 0x80) && ((mp->acc ^ mp->mdr) & 0x80)));

    result = (mp->acc & 0x0F) - (mp->mdr & 0x0F) - (!oc);

    // Correct first nimble
    if (result < 0) result = ((result - 6) & 0x0F) - 0x10;

    result = (mp->acc & 0xF0) - (mp->mdr & 0xF0) + result;

    // Correct second nimble
    if (result < 0) result -= 0x60;

    goto assing_result;
  }
#endif

  set_status(mp, kC, (uint16_t)result < 0x100);
  update_zn(mp, (uint8_t)result);
  set_status(mp, kV,
             (((mp->acc ^ result) & 0x80) && ((mp->acc ^ mp->mdr) & 0x80)));

assing_result:
  mp->acc = (uint8_t)result;
}

void bets6502_jam(bets6502* mp) {
  mp->mar = 0xFF;
  mp->mdr = 0xFF;
  mp->is_jammed = true;
}

void bets6502_lax(bets6502* mp) {
  // T1 - Read data from memory
  mp->acc = mp->x = bets6502_read_inl(mp, mp->mar);
}

void bets6502_rla(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  bets6502_tick(mp);             // T2 - Read (dummy)

  const uint8_t oc = (uint8_t)get_status(mp, kC);
  set_status(mp, kC, mp->mdr & 0x80);

  mp->mdr = (uint8_t)(mp->mdr << 1) | oc;    // T3/0 - Modify
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  mp->acc &= mp->mdr;
  update_zn(mp, mp->acc);
}

void bets6502_rra(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  bets6502_tick(mp);             // T2 - Read (dummy)

  const uint8_t oc = (uint8_t)((uint8_t)get_status(mp, kC) << 7);
  set_status(mp, kC, mp->mdr & 1);

  mp->mdr = (uint8_t)(mp->mdr >> 1 | oc);    // T3/0 - Modify
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  uint16_t register result = mp->acc + mp->mdr + (uint8_t)get_status(mp, kC);

#ifdef BETS6502_WITH_DECIMAL_MODE
  if (get_status(mp, kD)) {
    set_status(mp, kZ, (uint8_t)result == 0);

    result = (mp->acc & 0x0F) + (mp->mdr & 0x0F) + (uint8_t)get_status(mp, kC);

    // Fix lower nibble.
    if (result > 0x09) result = ((result + 6) & 0x0F) + 0x10;

    result = (mp->acc & 0xF0) + (mp->mdr & 0xF0) + result;

    set_status(mp, kN, (result & 0x80));
    set_status(mp, kV, ~(mp->acc ^ mp->mdr) & (mp->acc ^ result) & 0x80);

    // Fix higher nibble.
    if (result > 0x9F) result += 0x60;

    set_status(mp, kC, result > 0x9F);

    goto assing_result;
  }
#endif

  set_status(mp, kC, result > 0xFF);
  update_zn(mp, (uint8_t)result);
  set_status(mp, kV, ~(mp->acc ^ mp->mdr) & (mp->acc ^ result) & 0x80);

assing_result:
  mp->acc = (uint8_t)result;
}

void bets6502_sax(bets6502* mp) {
  // T1 - Write A & X to memory
  bets6502_write_inl(mp, mp->mar, mp->acc & mp->x);
}

void bets6502_sbx(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address

  mp->x &= mp->acc;
  set_status(mp, kC, mp->x >= mp->mdr);

  mp->x -= mp->mdr;
  update_zn(mp, mp->x);
}

void bets6502_slo(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  bets6502_tick(mp);             // T2 - Read (dummy)

  set_status(mp, kC, mp->mdr & 0x80);

  mp->mdr = (uint8_t)(mp->mdr << 1);         // T3/0 - Modify
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  mp->acc |= mp->mdr;
  update_zn(mp, mp->acc);
}

void bets6502_sre(bets6502* mp) {
  bets6502_fetch(mp, &mp->mdr);  // T1 - Fetch data from effective address
  bets6502_tick(mp);             // T2 - Read (dummy)

  set_status(mp, kC, mp->mdr & 1);

  mp->mdr = mp->mdr >> 1;                    // T3/0 - Modify
  bets6502_write_inl(mp, mp->mar, mp->mdr);  // T3/1 - Write

  mp->acc ^= mp->mdr;
  update_zn(mp, mp->acc);
}
