cmake_minimum_required(VERSION 3.11)
project("bets6502Lib"
        DESCRIPTION "6502 Emulator, lib version"
        LANGUAGES C)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(bus_header_list "${ROOT_INCLUDE_DIR}/bets6502/bus.h"
                    "${ROOT_INCLUDE_DIR}/bets6502/bus/mmu.h"
                    "${ROOT_INCLUDE_DIR}/bets6502/bus/memory.h"
                    "${ROOT_INCLUDE_DIR}/bets6502/bus/history.h")

set(bus_src_list "bus.c"
                 "bus/mmu.c"
                 "bus/memory.c"
                 "bus/history.c")

add_library(bets6502 STATIC "")
target_sources(bets6502 PRIVATE
               "6502.c" "error.c" ${bus_src_list} PUBLIC
               "${ROOT_INCLUDE_DIR}/bets6502/6502.h"
               "${ROOT_INCLUDE_DIR}/bets6502/error.h"
               ${bus_header_list})

target_include_directories(bets6502 PUBLIC "${ROOT_INCLUDE_DIR}")

if(BETS6502_WITH_DECIMAL_MODE)
   target_compile_definitions(bets6502 PRIVATE BETS6502_WITH_DECIMAL_MODE=1)
endif()

message("Setting compile options for ${CMAKE_BUILD_TYPE}...")
target_compile_options(bets6502 PRIVATE ${DEFAULT_COMPILE_OPTIONS})
set_target_properties(bets6502 PROPERTIES LINKER_LANGUAGE C)

if(CMAKE_BUILD_TYPE MATCHES "Release")
    target_compile_options(bets6502 PRIVATE ${DEFAULT_COMPILE_OPTIONS} "/O2" "/FA")
elseif(CMAKE_BUILD_TYPE MATCHES "Debug")
   target_compile_options(bets6502 PRIVATE
   ${DEFAULT_COMPILE_OPTIONS}
   ${DEBUG_COMPILE_OPTIONS}
   )

endif()

message("${CMAKE_BUILD_TYPE} compile options set.")

source_group("CPU" FILES 6502.c "${ROOT_INCLUDE_DIR}/bets6502/6502.h")
source_group("Bus" FILES ${bus_src_list} ${bus_header_list})