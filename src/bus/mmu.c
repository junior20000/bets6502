#include "bets6502/bus/mmu.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>
#include <string.h>

#include "bets6502/error.h"

struct betsMMU* betsMMU_new(size_t capacity) {
  if (!capacity) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "MMU capacity must be greater than zero.");

    return NULL;
  }

  size_t reg_sz = capacity * sizeof(struct betsRegion);

  struct betsMMU* const mmu = malloc(sizeof(struct betsMMU) + reg_sz);

  if (!mmu) {
    bets6502_set_error(BETS6502_OUT_OF_MEMORY, "Could not allocate the MMU.");

    return NULL;
  }

  *(size_t*)&mmu->capacity = capacity;
  mmu->size = 0;
  memset(&mmu->regions, 0, reg_sz);

  return mmu;
}

bool betsMMU_regions_is_sorted(const struct betsMMU* mmu) {
  if (mmu->size < 2) {
    return true;
  }

  for (size_t i = 0; i < mmu->size - 1; i++) {
    if (mmu->regions[i].mint.begin > mmu->regions[i + 1].mint.begin) {
      return false;
    }
  }

  return true;
}

void betsMMU_sort_regions(struct betsRegion* arr, size_t low, size_t high) {
  assert(arr);

  if (low > high) {
    bets6502_set_error(BETS6502_INVALID_MEMORY_RANGE,
                       "Region start cannot be greater than the end.");

    return;
  }

  if (low == high) {
    return;
  }

  for (size_t i = low + 1; i <= high; i++) {
    struct betsRegion vma = arr[i];
    size_t j;

    for (j = i; (j > low) && (arr[j - 1].mint.begin > vma.mint.begin); j--) {
      memcpy_s(&arr[j], sizeof(struct betsRegion), &arr[j - 1],
               sizeof(struct betsRegion));
    }
    memcpy_s(&arr[j], sizeof(struct betsRegion), &vma,
             sizeof(struct betsRegion));
  }
}

void betsMMU_build(struct betsMMU* mmu, const struct betsRegion* arr,
                   size_t arr_start, size_t arr_end) {
  if (!mmu || !arr) {
    bets6502_set_error(BETS6502_NULL_POINTER,
                       "Null pointer passed as argument.");

    return;
  }

  if (arr_start > arr_end) {
    bets6502_set_error(BETS6502_INVALID_MEMORY_RANGE,
                       "Array start cannot be greater than the end.");

    return;
  }

  if (!mmu->capacity) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "MMU capacity must be greater than zero.");

    return;
  }

  if ((arr_end - arr_start) > mmu->capacity) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "Array size overwhelms the MMU's capacity.");

    return;
  }

  for (size_t i = arr_start; i <= arr_end; i++) {
    memcpy_s(&mmu->regions[i], sizeof(struct betsRegion), &arr[i],
             sizeof(struct betsRegion));
    mmu->size++;
  }
}

void betsMMU_build_sorted(struct betsMMU* mmu, const struct betsRegion* regions,
                          size_t rstart, size_t rend) {
  betsMMU_build(mmu, regions, rstart, rend);
  betsMMU_sort_regions(mmu->regions, rstart, rend);
}

void betsMMU_build_s(struct betsMMU* mmu, const struct betsRegion* regions,
                     size_t rstart, size_t rend) {
  if (!mmu || !regions) {
    bets6502_set_error(BETS6502_NULL_POINTER,
                       "Null pointer passed as argument.");

    return;
  }

  if (rstart > rend) {
    bets6502_set_error(BETS6502_INVALID_MEMORY_RANGE,
                       "Region start cannot be greater than the end.");

    return;
  }

  if (!mmu->capacity) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "MMU capacity must be greater than zero.");

    return;
  } else if (!mmu->regions) {
    bets6502_set_error(BETS6502_INVALID_SIZE, "Region array is not allocated.");

    return;
  }

  if ((rend - rstart) > mmu->capacity) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "Array size overwhelms the MMU's capacity.");

    return;
  }

  for (size_t i = rstart; i <= rend; i++) {
    if ((betsMMU_get_region_at(mmu, regions[i].mint.end) ||
         betsMMU_get_region_at(mmu, regions[i].mint.begin))) {
      bets6502_set_error(BETS6502_REGION_OVERLAP, "Regions must not overlap.");

      return;
    }

    memcpy_s(&mmu->regions[i], sizeof(struct betsRegion), &regions[i],
             sizeof(struct betsRegion));
    mmu->size++;

    betsMMU_sort_regions(mmu->regions, 0, mmu->size - 1);
  }
}

uint16_t betsMMU_register_region(struct betsMMU* mmu, uint16_t begin,
                                 uint16_t end, const char* id, void* const obj,
                                 iofn_rd io_rd, iofn_wr io_wr) {
  assert(mmu);
  assert(mmu->size < mmu->capacity);
  assert(begin <= end);

  *(uint16_t*)&mmu->regions[mmu->size].mint.begin = begin;
  *(uint16_t*)&mmu->regions[mmu->size].mint.end = end;
  mmu->regions[mmu->size].obj = obj;
  mmu->regions[mmu->size].io_rd = io_rd;
  mmu->regions[mmu->size].io_wr = io_wr;

  strcpy_s((char*)mmu->regions[mmu->size].id, 4, id);

  mmu->size++;

  return 1;
}

uint16_t betsMMU_register_region_sorted(struct betsMMU* mmu, uint16_t begin,
                                        uint16_t end, const char* id,
                                        void* const obj, iofn_rd io_rd,
                                        iofn_wr io_wr) {
  betsMMU_register_region(mmu, begin, end, id, obj, io_rd, io_wr);
  betsMMU_sort_regions(mmu->regions, 0, mmu->size - 1);

  return 1;
}

uint16_t betsMMU_register_region_s(struct betsMMU* mmu, uint16_t begin,
                                   uint16_t end, const char* id,
                                   void* const obj, iofn_rd io_rd,
                                   iofn_wr io_wr) {
  assert(
      !(betsMMU_get_region_at(mmu, begin) || betsMMU_get_region_at(mmu, end)));
  betsMMU_register_region_sorted(mmu, begin, end, id, obj, io_rd, io_wr);

  return 0;
}

struct betsRegion* betsMMU_get_region_at(struct betsMMU* mmu, uint16_t addr) {
  assert(mmu);

  if (!mmu->size) {
    return NULL;
  }

  for (size_t i = 0; i < mmu->size; i++) {
    struct betsRegion* reg = &mmu->regions[i];

    if (addr >= reg->mint.begin && addr <= reg->mint.end) {
      return reg;
    }
  }

  return NULL;
}

void* betsMMU_get_obj_at(struct betsMMU* mmu, uint16_t addr) {
  struct betsRegion* vma = betsMMU_get_region_at(mmu, addr);

  return vma ? vma->obj : NULL;
}