#include <assert.h>
#include <bets6502/bus/memory.h>
#include <bets6502/error.h>
#include <memory.h>
#include <stdlib.h>

struct betsMemory* betsMemory_new(size_t size) {
  if (!size) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "Could not allocate the memory. Memory size must be "
                       "greater than zero.");

    return NULL;
  }

  struct betsMemory* mem =
      malloc(sizeof(struct betsMemory) + (size * sizeof(uint8_t)));

  if (!mem) {
    bets6502_set_error(BETS6502_OUT_OF_MEMORY,
                       "Could not allocate the memory.");

    return mem;
  }

  *(size_t*)&mem->sz = size;
  memset(mem->data, 0, size);

  return mem;
}

void betsMemory_clear(struct betsMemory* mem) {
  assert(mem);

  memset(mem->data, 0, mem->sz);
}
