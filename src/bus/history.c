#include "bets6502/bus/history.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

#include "bets6502/error.h"

struct betsBusHistory* betsBusHistory_new(size_t size) {
  if (!size) {
    bets6502_set_error(BETS6502_INVALID_SIZE,
                       "Buffer size must be greater than zero.");

    return NULL;
  }

  struct betsBusHistory* buff = malloc(sizeof(struct betsBusHistory) +
                                       (size * sizeof(struct betsBusActivity)));

  if (!buff) {
    bets6502_set_error(
        BETS6502_OUT_OF_MEMORY,
        "Could not allocate the buffer. Null pointer passed as argument.");

    return NULL;
  }

  buff->sz = size;
  buff->rptr = 0;
  buff->wptr = 0;

  memset(buff->data, 0, size * sizeof(struct betsBusActivity));

  return buff;
}

void betsBusHistory_add(struct betsBusHistory* hist, bool rw, uint16_t addr,
                        uint8_t val) {
  assert(hist);

  struct betsBusActivity* act = &hist->data[hist->wptr++];
  act->addr = addr;
  act->val = val;
  act->rw = rw;

  if (hist->wptr == hist->sz) {
    hist->wptr = 0;
  }

  if (hist->wptr == hist->rptr) {
    hist->rptr++;
    if (hist->rptr == hist->sz) {
      hist->rptr = 0;
    }
  }
}

struct betsBusActivity* betsBusHistory_get(struct betsBusHistory* hist) {
  assert(hist);

  if (hist->rptr == hist->wptr) {
    return NULL;
  }

  struct betsBusActivity* const act = &hist->data[hist->rptr++];
  if (hist->rptr == hist->sz) {
    hist->rptr = 0;
  }

  return act;
}

void betsBusHistory_clear(struct betsBusHistory* hist) {
  assert(hist);

  if (!hist->sz) {
    return;
  }

  memset(hist->data, 0, hist->sz * sizeof(struct betsBusActivity));
}

void betsBusHistory_reset(struct betsBusHistory* hist) {
  betsBusHistory_clear(hist);

  hist->rptr = 0;
  hist->wptr = 0;
}
