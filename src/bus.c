#include "bets6502/bus.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

#include "bets6502/error.h"

void betsBus_init(Bus_t* bus, size_t mem_sz, size_t num_regions,
                  size_t hist_len) {
  if (!bus) {
    bets6502_set_error(
        BETS6502_NULL_POINTER,
        "Could not initialize the bus. Null pointer passed as argument.");

    return;
  }

  bus->mem = betsMemory_new(mem_sz);

  if (bets6502_get_error()) {
    return;
  }

  bus->hist = betsBusHistory_new(hist_len);

  if (bets6502_get_error()) {
    goto err_hist;
  }

  bus->mmu = betsMMU_new(num_regions);

  if (bets6502_get_error()) {
    goto err_mmu;
  }

  return;

err_mmu:
  free(bus->hist);
err_hist:
  free(bus->mem);
}

void betsBus_init1(Bus_t* bus, size_t num_regions, size_t hist_len,
                   struct betsMemory* mem) {
  if (!bus || !mem) {
    bets6502_set_error(
        BETS6502_NULL_POINTER,
        "Could not initialize the bus. Null pointer passed as argument.");

    return;
  }

  bus->mem = mem;
  bus->mmu = betsMMU_new(num_regions);

  if (bets6502_get_error()) {
    return;
  }

  bus->hist = betsBusHistory_new(hist_len);

  if (bets6502_get_error()) {
    goto err;
  }

  return;

err:
  free(bus->mmu);
}

void betsBus_free(Bus_t* bus) {
  if (!bus) {
    bets6502_set_error(
        BETS6502_NULL_POINTER,
        "Could not free the bus. Null pointer passed as argument.");

    return;
  }

  free(bus->mmu);
  free(bus->mem);
  free(bus->hist);

  bus->mem = NULL;
}

void betsBus_free1(Bus_t* bus) {
  if (!bus) {
    bets6502_set_error(
        BETS6502_NULL_POINTER,
        "Could not free the bus. Null pointer passed as argument.");

    return;
  }

  free(bus->mmu);
  free(bus->hist);
}

void betsBus_write(const Bus_t* bus, uint16_t addr, uint8_t data) {
  assert(bus);  // TODO: Study if I should check for NULL in this case.

  struct betsRegion* reg = betsMMU_get_region_at(bus->mmu, addr);

  if (reg) {
    (*reg->io_wr)(reg->obj, addr, data);
    betsBusHistory_add(bus->hist, false, addr, data);
  }
}

uint8_t betsBus_read(const Bus_t* bus, uint16_t addr) {
  assert(bus);  // TODO: Will think on a better error handler option later.
                // Maybe a struct?

  struct betsRegion* reg = betsMMU_get_region_at(bus->mmu, addr);

  if (reg) {
    uint8_t data = (*reg->io_rd)(reg->obj, addr);
    betsBusHistory_add(bus->hist, true, addr, data);
    return data;
  }

  return 0;
}
